[![Status](resources/images/badges/badge_status_active.png)](https://codeberg.org/shufflecake/shufflecake-c)&nbsp;
[![Version](resources/images/badges/badge_version_0.4.4.png)](https://codeberg.org/shufflecake/shufflecake-c/releases/tag/v0.4.4)&nbsp;
[![License](resources/images/badges/badge_license_gplv2plus.png)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)&nbsp;
[![Docs researchpaper](resources/images/badges/badge_docs_researchpaper.png)](https://eprint.iacr.org/2023/1529)&nbsp;
[![Website](resources/images/badges/badge_web_shufflecakedotnet.png)](https://shufflecake.net/)&nbsp;
[![Issue tracker](resources/images/badges/badge_community_issuetracker.png)](https://codeberg.org/shufflecake/shufflecake-c/issues)&nbsp;
[![Mastodon](resources/images/badges/badge_community_mastodon.png)](https://fosstodon.org/@shufflecake)&nbsp;
[![Please don't upload to GitHub](resources/images/badges/badge_no_github.png)](https://nogithub.codeberg.page)





# Shufflecake - Full C Implementation - v0.4.4

_Shufflecake_ is a plausible deniability (hidden storage) layer for Linux. You can consider Shufflecake a spiritual successor of tools like TrueCrypt and VeraCrypt, but vastly improved, both in terms of security and functionality. Official website: <https://www.shufflecake.net>.

This repository contains C source code and documentation to use and manage Shufflecake volumes.

__WARNING__: Shufflecake is still experimental software, please do not rely on its security or stability.

- [Overview](#overview)
- [Installation](#installation)
    - [Tests](#tests)
    - [Benchmarks](#benchmarks)
- [Usage](#usage)
    - [Initializing Device](#init)
    - [Opening Volumes](#open)
    - [Closing Volumes](#close)
    - [Other Commands](#other)
      - [Changing A Password](#changepwd)
      - [Check If A Password Is Correct](#testpwd)
    - [Advanced Usage](#advanced)
      - [Volume Corruption Mitigation](#mitigcorr)
      - [Providing Passwords Non-Interactively](#passwordnonint)
- [Changelog](#changelog)
- [Bugs](#bugs)
- [Maintainers](#maintainers)
- [Copyright](#copyright)





## Overview <a id="user-content-overview"></a>

In the context of Shufflecake, a _device_, or _cake_, is the underlying raw block device (e.g., a disk) that is formatted to contain hidden data, while a _volume_, or _layer_, is the logical, encrypted and hidden "partition" within a device. The device can be a whole USB stick (or disk), a physical or logical partition, a file-backed loop device, etc. (you likely find it under `/dev`).

The three operating principles of Shufflecake are:
- 1 device = multiple volumes;
- 1 volume = 1 password = 1 "secrecy level";
- unlocking a volume also unlocks all those of lesser secrecy level.

Volumes are password-protected, and embedded in the underlying device as data _slices_ which are indistinguishable from random noise without the proper password. Headers are also indistinguishable from random when not decrypted. A Shufflecake-initialized device does not have cleartext headers, and is indistinguishable from random noise when not decrypted.

Up to 15 _ordered_ Shufflecake volumes can be created on a single device, with the implicit assumption that "lower-order" volumes (e.g. layer 0) are _less_ _secret_ than "higher-order" ones (e.g. layer 3). The Shufflecake header is designed in such a way that it _chains_ volumes in a backwards-linked list: volume __i__ "points to" (contains the key of) volume __i-1__. This way, providing the key of volume __i__ allows this tool to traverse the list and also automatically open volumes __0__ through __i-1__ (besides volume __i__). 

![Scheme of Shufflecake device layout](resources/images/headers.png)

Opened volumes appear as block devices of the form `sflc_x_y` in `/dev/mapper`. It is up to the user to format them with an appropriate filesystem and mounting them before use. It is recommended to always unlock the most sensitive volume for daily use ("home alone" scenario), even if that volume is not being used or even mounted. Only open a subset of volumes (with a "decoy password") if under coercion. This is due to the high possibility of corrupting hidden volumes otherwise.

For security and consistency reasons, you cannot init/open/close nested volumes within the same device one at a time; this tool only allows to perform these operations on a _chain_ of volumes in a device.





## Installation <a id="user-content-installation"></a>

This implementation of Shufflecake consists of two components: a module for the Linux kernel (`dm-sflc`), and a `shufflecake` userland tool. Both are necessary in order to use Shufflecake. They have been tested on Linux kernel versions 6.1 (LTS), up to 6.6. The following instructions are given for Debian/Ubuntu and similar derivatives.

First of all, you need the kernel headers, device-mapper userspace library, and libgcrypt to compile the source. Use:

```
sudo apt install linux-headers-$(uname -r) libdevmapper-dev libgcrypt-dev
```

Important: you have to make sure to install an up-to-date version of `libgcrypt` that supports the Argon2id KDF algorithm. You need the 1.10.1 or later version, which might not be available in your local repositories (for example, Ubuntu 22.04 LTS), in which case you should find a workaround (either install manually or override your repositories by pinning an up-to-date package). You can check your current version with:

```
libgcrypt-config --version
```
Also, make sure that the Kconfig options `CONFIG_CRYPTO_DRBG_HASH` (and possibly `CONFIG_CRYPTO_DRBG_CTR`) are enabled, as they are not default options in the kernel's defconfig, although they are enabled by default on some distributions such as Debian and Ubuntu.

After that, just run `make`. All the compilation artifacts will go in the respective subdirectories, and a copy of the kernel module `dm_sflc.ko` and the userland tool `shufflecake` will appear in the root directory. You can clean (delete) all these artifacts with `make clean`.

For now there is no `make install` option, if you want you can install Shufflecake system-wide manually, by moving the executable to the appropriate directory (e.g. `/usr/bin`) and making sure that the kernel module is loaded automatically at boot.


### Tests <a id="user-content-tests"></a>

Limited tests are provided to check pre-install consistency, for now they only test the cryptography layer. Run `make test` to compile and run them. Remember to `make clean` afterwards.


### Benchmarks <a id="user-content-benchmarks"></a>

A complete benchmark suite is provided in the `benchmark-suite` directory, to test the performance of Shufflecake on a given system and against other tools such as LUKS and VeraCrypt. Please refer to the documentation in that directory for instructions on how to run the benchmark scripts.





## Usage <a id="user-content-usage"></a>

The `shufflecake` tool requires that the `dm-sflc` kernel module is already loaded. Therefore, first of all you must load the kernel module with:

```
sudo insmod dm-sflc.ko
```

At this point you can run the `shufflecake` command as `sudo shufflecake <action> <block_device>`.

When finished, you can remove the module with 

```
sudo rmmod dm-sflc
```

You can only do this if no Shufflecake volume is open.


### Initializing Device <a id="user-content-init"></a>

```
sudo shufflecake init <block_device>
```

This command creates __N__ volume headers on the given device, each sealed by the respective provided password, by properly formatting and encrypting the first __N__ volume header slots. The number of desired volumes __N__ and the related __N__ passwords will be interactively asked at prompt. Alternatively, you can pass __N__ by command line with:

```
sudo shufflecake --num-volumes=N init <block_device>
```

__WARNING__: If the device is not empty, you will lose all data stored therein. Also, adding additional volumes after initialisation is not yet supported.

This command does not open the volumes (it does not create the virtual devices under `/dev/mapper/`), it only overwrites the device with randomness and formats the encrypted headers.


### Opening Volumes <a id="user-content-open"></a>

```
sudo shufflecake open <block_device>
```

A password will be required. If this is the password for volume __M__, then this command will open the first __M__ volumes on the device. These will appear as virtual devices under `/dev/mapper/`, named `sflc_<devID>_0` through `sflc_<devID>_(M-1)`, where `devID` is a Shufflecake-unique numeric ID assigned by the kernel module (`dm_sflc`) to the block device.

Notice that at the beginning all these volumes will be unformatted; in order to use them to store files, once created for the first time you must format them with an appropriate filesystem, for example with:

```
sudo mkfs.ext4 /dev/mapper/sflc_x_y
```

Also notice that Shufflecake does not auto-mount the volumes, you have to do it manually in order to see and manipulate their content, for example with:

```
sudo mount /dev/mapper/sflc_x_y /media/yourusername/sflc_x_y
```


### Closing Volumes <a id="user-content-close"></a>

```
sudo shufflecake close <block_device>
```

Closes all the volumes currently open on a device, removing them from `/dev/mapper/`.

The command only asks for the block device name, i.e., it will close _all_ volumes provided by that device at once, by first forcing a write on disk of any pending changes. However, in order to avoid any risk of data loss, it is always advisable to first `umount` any mounted open volume, for example with:

```
sudo umount /media/yourusername/sflc_x_y
```


### Other Commands <a id="user-content-other"></a>

The following commands implement additional useful features.

#### Changing a Password <a id="user-content-changepwd"></a>

```
sudo shufflecake changepwd <block_device>
```

A password will be required. If this is the correct password for a volume within that device, then the user will be prompted to enter a new password for that volume. Shufflecake will only modify a single ciphertext field in the header of that volume, without need of re-encrypting the volume itself.

#### Check if a Password is Correct <a id="user-content-changepwd"></a>

```
sudo shufflecake testpwd <block_device>
```

A password will be required. If this is the correct password for volume __M__, then this information will be returned to the user, without actually opening the volume. This can be useful if a user wants to be sure that a password unlocks a certain volume without risk of the OS logging the access to that volume and without the risk of corrupting the content of other, unlocked, hidden volumes.


### Advanced Usage <a id="user-content-advanced"></a>

Certain features of Shufflecake are not yet implemented, but sometimes a workaround is possible. Consider all the instructions in this sections as experimental, use them at your own risk.


#### Volume Corruption Mitigation <a id="user-content-mitigcorr"></a>

As explained in the <a href="https://shufflecake.net/#faqcorruptionunopened">FAQ</a>, if not all hidden volumes are opened and the user writes data to decoy volumes, there is a risk of data loss on the unopened hidden volumes (so-called "volume corruption"). This is because Shufflecake will try to allocate new slices for the decoy volumes by picking positions at random across all the (unopened) available space. There is ongoing research to mitigate this risk by using some form of redundancy on the volumes, basically by using error-correcting codes that can self-heal from a certain degree of corruption by sacrificing some disk space. In the meantime, users can resort to a "poor man's implementation" of this idea by using RAID redundancy on a partitioned hidden volume. This is still quite "hacky", and at the very minimum should be automated with some scripting.

More in detail:

1. Open all Shufflecake volumes on a device, e.g. `sflc_0_0` and `sflc_0_1` in the case of two volumes (one decoy and one hidden).

2. Partition the volume you want to protect (say, `sflc_0_1`) into different partitions using, e.g., `fdisk` or `parted`. Let's say you obtain 3 equal-sized partitions named `sflc_0_11, sflc_0_12` and `sflc_0_13`.

3. Use `mdadm` to build a redundant RAID array on these volumes. For example, if using RAID-1, you should use:
```
sudo mdadm --create --verbose /dev/md0 --level=1 --raid-devices=3 /dev/mapper/sflc_0_11 /dev/mapper/sflc_0_12 /dev/mapper/sflc_0_13

```
You can verify the creation of the RAID array with:
```
cat /proc/mdstat
```
__WARNING__: *do not* save the current RAID configuration to `/etc/mdadm/mdadm.conf`, otherwise it will super-obviously break plausible deniability.

4. Format `/dev/md0` with a filesystem of your choice, mount it, and use it.

5. After umounting it, remember to disassemble the RAID device before doing `shufflecake close`.

You can use different redundant RAID systems such as RAID-5 (minimum 3 volumes) or RAID-6 (minimum 4 volumes, recommended 5 or more), each of them with different tradeoffs in terms of space wasted, speed, and corruption resilience. In any case there is no 100% guarantee that unopened hidden volumes will not be corrupted when writing data on decoys, so the recommended course of action for the user is still to open *all* devices for daily use (but mounting them is not mandatory). This method is intended to offer a certain degree of convenience for the user when recovering data in a hidden volume after an adversarial investigation, i.e., having some chance of recovering "gracefully" instead of having to restore from a backup.


#### Providing Passwords Non-Interactively <a id="user-content-passwordnonint"></a>

By default `shufflecake init` will ask the user for the number of volumes to create and the sequence of passwords. The number of volumes can be passed as argument with the `-n` option, but there is currently no built-in mechanism to provide passwords non-interactively, which might be useful, e.g., for scripting. However, there is a simple workaround. As an example with 3 volumes:
```
echo -e "password1\npassword2\npassword3" | sudo ./shufflecake -n 3 init <block_device>
```
Even better, you can put `password0` ... `password2` as the first three lines in a textfile, say `passwords.txt`, and pass them with:
```
sudo ./shufflecake -n 3 init <block_device> < passwords.txt
```
Both methods works with the `init` action, and we do not have current plans to change this behavior. Notice, however, that there is no guarantee that this will keep working with `open` or other actions, because the password fetching interface there could be supposed to be more robust, and its implementation might change in the future.





## Changelog <a id="user-content-changelog"></a>

Please see the file `CHANGELOG.md` for a detailed history of changes.

### [0.4.4] - 2023-12-31

 - Fixed a memory allocation error on large devices.
 
### [0.4.3] - 2023-11-29

 - Fixed compile error on recent kernel versions.
 
### [0.4.2] - 2023-11-28

 - Fixed persistent slice allocation ambiguity after a volume corruption by allocating fresh slices for the corrupted volume. This is done in order to help external recovery tools (e.g. RAID).
 - Various bugfixes.

### [0.4.1] - 2023-07-30

 - Fixed and improved benchmark scripts.
 - Fixed mistake in drawing of header layout in `doc`.

### [0.4.0] - 2023-07-24

- BREAKING CHANGE: slightly modified header field format, removing redundant data and making it adherent to documentation.
- Implemented reference slice allocation algorithm with much faster performance.
- Added actions `checkpwd` and `changepwd`.
- Password is now read in a secure shell during `open`.
- Added benchmark suite.
- Fixed bugs in action `close` and option `--skip-randfill`.
- Added `doc` (currently includes figure of Shufflecake headers structure).





## Bugs <a id="user-content-bugs"></a>

Bugs and other issues are tracked at <https://codeberg.org/shufflecake/shufflecake-c/issues>, please report new bugs by submitting new issues there. Alternatively, you can send an email to <bugs@shufflecake.net>.

Outstanding known bugs:

 - There is no crash consistency implemented, so if a crash happens while some volumes are live there is the risk of data loss.
 - There is no garbage collection currently implemented, so slices remain flagged as used even if the data therein is deleted.





## Maintainers <a id="user-content-maintainers"></a>

Elia Anzuoni <elianzuoniATgmail.com>

Tommaso "Tomgag" Gagliardoni <tommasoATgagliardoni.net>





## Copyright <a id="user-content-copyright"></a>

Copyright The Shufflecake Project Authors (2022)

Copyright The Shufflecake Project Contributors (2022)

Copyright Contributors to the The Shufflecake Project.

See the AUTHORS file at the top-level directory of this distribution and at <https://www.shufflecake.net/permalinks/shufflecake-c/AUTHORS>.

The program shufflecake-c is part of the Shufflecake Project. Shufflecake is a plausible deniability (hidden storage) layer for Linux. See <https://www.shufflecake.net>.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
    

