# Security Policy

This file contains guidelines and policies for reporting and addressing security vulnerabilities of shufflecake-c. The program shufflecake-c is part of the Shufflecake Project. Shufflecake is a plausible deniability (hidden storage) layer for Linux. See <https://www.shufflecake.net>.

## Reporting a Vulnerability

Please notice that Shufflecake is still experimental, so we are not managing vulnerabilities in a confidential manner at this stage. In the future we will add information to confidentially report suspected vulnerabilities. For now, any vulnerability should be considered a bug and reported in the git issue tracker.

<!---
Please report (suspected) security vulnerabilities to **[securityATshufflecake.net](mailto:securityATshufflecake.net)**. In the future we will add a dedicated PGP key to allow you to encrypt communication to this address. GPG signing of your communication is welcome, but you can also report anonymously.

You will receive a response from us as soon as possible. If the issue is confirmed, we will release a patch as soon as possible. You can be acknowledged as reporter of the bug, depending on the severity of the vulnerability and if you agree.
--->


