#!/bin/bash

#  Copyright The Shufflecake Project Authors (2022)
#  Copyright The Shufflecake Project Contributors (2022)
#  Copyright Contributors to the The Shufflecake Project.
  
#  See the AUTHORS file at the top-level directory of this distribution and at
#  <https://www.shufflecake.net/permalinks/shufflecake-c/AUTHORS>
  
#  This file is part of the program shufflecake-c, which is part of the Shufflecake 
#  Project. Shufflecake is a plausible deniability (hidden storage) layer for 
#  Linux. See <https://www.shufflecake.net>.
  
#  This program is free software: you can redistribute it and/or modify it 
#  under the terms of the GNU General Public License as published by the Free 
#  Software Foundation, either version 2 of the License, or (at your option) 
#  any later version. This program is distributed in the hope that it will be 
#  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
#  Public License for more details. You should have received a copy of the 
#  GNU General Public License along with this program. 
#  If not, see <https://www.gnu.org/licenses/>.

# Benchmarking script for VeraCrypt

# Variables
SCRIPTNAME=$(basename "$0")
SCRIPT_DIR="$(dirname "$(realpath "$0")")"
CONTAINER_FILENAME=""
TIMEFORMAT='%3R'

# Colors
BLUE='\033[0;34m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No color

# Help
print_help() {
#         xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   79 chars
    echo -e "${BLUE}Usage: ${SCRIPTNAME} [OPTION]... [BLOCKDEVICE]${NC}"
    echo " "
    echo "This script is used to benchmark VeraCrypt on this machine."
    echo "This script is part of the Shufflecake benchmark suite."
    echo "Shufflecake is a plausible deniability (hidden storage) layer for Linux."
    echo -e "Please visit ${BLUE}https://www.shufflecake.net${NC} for more info and documentation."
    echo " "
    echo "This script requires root because it operates on block devices, please run it "
    echo -e "with ${BLUE}sudo${NC}. It does the following:"
    echo "1) Creates a standard and unformatted VeraCrypt volumes within a given device."
    echo "2) Creates a hidden ext4 VeraCrypt volume within the standard one."
    echo "3) Opens the hidden volume, and mounts it."
    echo "4) Performs various fio r/w stress operations on it."
    echo "5) Unmounts and closes the used volume."
    echo " "
#         xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   79 chars
    echo "You can pass the path to a (non-loop) block device as an optional argument, "
    echo "otherwise the script will ask for one. If no path is provided, the script "
    echo "will create a 1 GiB VeraCrypt container file which will be removed at the end."
    echo "NOTICE: This script has been tested only with VeraCrypt v 1.25.9."
    echo " "
#         xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   79 chars
    echo -e "${BLUE}WARNING: ALL CONTENT OF THE PROVIDED BLOCK DEVICE WILL BE ERASED!${NC}"
    echo " "
    exit 0
}


# Function for debugging
bpx() {
    if [ -z "$1" ]; then
        echo "BPX: Paused. Press any key to continue..." >&2
    else
        echo "BPX: $1. Press any key to continue..." >&2
    fi
    read -n1 -s
}

# Show usage
usage() {
    echo -e "Use ${BLUE}${SCRIPTNAME} --help${NC} for usage and help."
}

# Check that this is run as root
check_sudo() {
    if [[ $EUID -ne 0 ]]; then
        echo -e "${RED}Error: This script must be run as root.${NC}"
        usage
        exit 1
    fi
}

# Function to check if argument is a block device
check_block_device() {
    if [ -b "$1" ]; then
        echo "OK, block device path $1 is valid." >&2
    else
        echo -e "${RED}Error: $1 is not a valid block device, aborting.${NC}"
        usage
        exit 1
    fi
}

check_not_loopdevice() {
    DEVCHECK=$1
    if [[ $DEVCHECK == /dev/loop* ]]; then
        echo -e "${RED}Error: $DEVCHECK is a loop device, this script does not work well with this type of devices due to how VeraCrypt works. Please either enter a non-loop block device, or just do not pass any path as input, this script will work on a container file directly.${NC}"
        exit 1
    fi
}

# Function to create container file
create_container_file() {
    CONTAINER_FILENAME="$SCRIPT_DIR/veracrypt-benchmark-container-file.img"
    echo "Creating an empty file $CONTAINER_FILENAME ..." >&2
    if [ -e "$CONTAINER_FILENAME" ]; then
        echo -e "${RED}Error: Impossible to generate file, $CONTAINER_FILENAME already exists.${NC}" >&2
        exit 1
    fi
    touch $CONTAINER_FILENAME
    #dd if=/dev/zero of="$CONTAINER_FILENAME" bs=1M count=1024 > /dev/null
    #echo "Writing of empty file complete." >&2
    echo "$CONTAINER_FILENAME"
}

# Function for user confirmation
confirm() {
    while true; do
        echo -e "${BLUE}Are you sure you want to proceed? All data in $BLOCK_DEVICE will be erased. (y/n)${NC}"
        read -r response
        case "$response" in
            [yY]|[yY][eE][sS])  # Responded Yes
                return 0        # Return 0 for Yes (success, convention for bash scripting)
                ;;
            [nN]|[nN][oO])  # Responded No
                return 1    # Return 1 for No (error, convention for bash scripting)
                ;;
            *)  # Responded something else
                echo "Please press only (y)es or (n)o."
                ;;
        esac
    done
}

# Benchmarks
benchmark() {

    MNTPOINT=""
    TESTNAME="vc"
    RUNTIME="20" # running time in seconds FOR EACH TEST
    DATASIZE="500M"
    TESTFILENAME="testfile"
    echo "Starting benchmark for VeraCrypt..."
    # Create a new standard volume
    if [ -z "$CONTAINER_FILENAME" ]; then
        # it's a real block device, do not specify size
        etime=$( (time echo "passdecoy" | veracrypt --text --non-interactive --quick --create $BLOCK_DEVICE --volume-type=normal --password - --encryption=AES --hash=SHA-512 --filesystem=None --random-source=/dev/urandom > /dev/null) 2>&1 )
    else
        # it's file-based, then VeraCrypt requires specifying the size
        etime=$( (time echo "passdecoy" | veracrypt --text --non-interactive --quick --create $BLOCK_DEVICE --size=1G --volume-type=normal --password - --encryption=AES --hash=SHA-512 --filesystem=None --random-source=/dev/urandom > /dev/null) 2>&1 )
    fi
    echo -e "${GREEN}Creation of standard VeraCrypt volume took $etime seconds.${NC}"
    # Create a hidden ext4 volume inside the previous one
    etime=$( (time echo "passhidden" | veracrypt --text --non-interactive --quick --create $BLOCK_DEVICE --volume-type=hidden --size="800M" --password - --encryption=AES --hash=SHA-512 --filesystem=ext4 --random-source=/dev/urandom > /dev/null) 2>&1 )
    echo -e "${GREEN}Creation of hidden VeraCrypt volume took $etime seconds.${NC}"
    # assign and create MNTPOINT
    MNTPOINT=$(realpath "./veracrypt_mnt")
    mkdir $MNTPOINT
    # mount hidden volume
    etime=$( (time echo -"passhidden" | veracrypt --text --non-interactive $BLOCK_DEVICE $MNTPOINT --password - > /dev/null) 2>&1 )
    echo -e "${GREEN}Opening and mounting hidden VeraCrypt volume took $etime seconds.${NC}"
    # TESTS HERE
    # test 01: random read
    echo "Test 01: random read with a queue of 32 4kiB blocks on a file (${RUNTIME}s)..."
    OUTPUT=$(fio --name=$TESTNAME-r-rnd --ioengine=libaio --iodepth=32 --rw=randread --bs=4k --numjobs=1 --direct=1 --size=$DATASIZE --runtime=$RUNTIME --time_based --end_fsync=1 --filename=$MNTPOINT/$TESTFILENAME --output-format=json | jq '.jobs[] | {name: .jobname, read_iops: .read.iops, read_bw: .read.bw}')
    echo -e "${GREEN}${OUTPUT}${NC}"
    # test 02: random write
    echo "Test 02: random write with a queue of 32 4kiB blocks on a file (${RUNTIME}s)..."
    OUTPUT=$(fio --name=$TESTNAME-w-rnd --ioengine=libaio --iodepth=32 --rw=randwrite --bs=4k --numjobs=1 --direct=1 --size=$DATASIZE --runtime=$RUNTIME --time_based --end_fsync=1 --filename=$MNTPOINT/$TESTFILENAME --output-format=json | jq '.jobs[] | {name: .jobname, write_iops: .write.iops, write_bw: .write.bw}')
    echo -e "${GREEN}${OUTPUT}${NC}"
    # test 03: sequential read
    echo "Test 03: sequential read with a queue of 32 4kiB blocks on a file (${RUNTIME}s)..."
    OUTPUT=$(fio --name=$TESTNAME-r-seq --ioengine=libaio --iodepth=32 --rw=read --bs=4k --numjobs=1 --direct=1 --size=$DATASIZE --runtime=$RUNTIME --time_based --end_fsync=1 --filename=$MNTPOINT/$TESTFILENAME --output-format=json | jq '.jobs[] | {name: .jobname, read_iops: .read.iops, read_bw: .read.bw}')
    echo -e "${GREEN}${OUTPUT}${NC}"
    # test 04: sequential write
    echo "Test 04: sequential write with a queue of 32 4kiB blocks on a file (${RUNTIME}s)..."
    OUTPUT=$(fio --name=$TESTNAME-w-seq --ioengine=libaio --iodepth=32 --rw=write --bs=4k --numjobs=1 --direct=1 --size=$DATASIZE --runtime=$RUNTIME --time_based --end_fsync=1 --filename=$MNTPOINT/$TESTFILENAME --output-format=json | jq '.jobs[] | {name: .jobname, write_iops: .write.iops, write_bw: .write.bw}')
    echo -e "${GREEN}${OUTPUT}${NC}"
    # END TESTS
    # close
    echo "Veracrypt fio tests ended. Detaching VeraCrypt volume..."
    etime=$( (time veracrypt -d $MNTPOINT > /dev/null) 2>&1 )
    echo -e "${GREEN}Action close took $etime seconds.${NC}"
    rmdir $MNTPOINT
    echo "Volume detached and local mountpoint removed."
    #end
}

# Clean up
cleanup() {
    echo "Exiting and cleaning..."
    if [[ -n $CONTAINER_FILENAME ]]; then
        echo "Deleting $CONTAINER_FILENAME..."
        rm -f "$CONTAINER_FILENAME"
        echo "Container file deleted."
    fi
}


#####################################################################

# MAIN SCRIPT BODY STARTS HERE

#####################################################################

# BANNER
#                xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   79 chars
echo -e "${BLUE}===============================================================================${NC}"
echo -e "${BLUE}                    Benchmark Suite Script for VeraCrypt${NC}"
echo -e "${BLUE}===============================================================================${NC}"


# PRELIMINARY: PARSE HELP, SUDO, AND CHECK VERACRYPT EXISTS

case "$1" in
	# help
    --help|-?)
        print_help
        exit 0
        ;;
esac

check_sudo

if ! which veracrypt >/dev/null; then
    echo -e "${RED}ERROR: VeraCrypt not found, please install it.${NC}"
    exit 1
fi

echo " "


# PARSER

case "$1" in
    "") # no argument passed
#             xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   79 chars

        echo "Now you will be asked to enter the path for a block device to be used for the "
        echo "benchmarks (all content will be erased). If no path is provided (default" 
        echo "choice), then the script will create a 1 GiB file in the current directory and "
        echo "use it a VeraCrypt container instead, then the file will be removed at the end."
        echo " "
        echo -n "Please enter the path for a (non-loop) block device (default: none): "
        read BLOCK_DEVICE
        if [ -z "$BLOCK_DEVICE" ]; then
            echo "No path provided, using a local container file."
            CONTAINER_FILENAME=$(create_container_file)
            if [ -z "$CONTAINER_FILENAME" ]; then
                exit
            fi
            BLOCK_DEVICE="$CONTAINER_FILENAME"
        else
            check_block_device "$BLOCK_DEVICE"
        fi
        
        ;;
        
        # argument passed
    *)
    	BLOCK_DEVICE="$1"
        check_block_device "$BLOCK_DEVICE"
        ;;
esac

check_not_loopdevice "$BLOCK_DEVICE"

# MAIN PROGRAM

if confirm; then
    benchmark
else
    echo "Aborting..."
fi

cleanup


