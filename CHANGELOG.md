# Changelog
This is the changelog for `shufflecake-c` which is part of the Shufflecake project.
Shufflecake is a plausible deniability (hidden storage) layer for Linux. See <https://www.shufflecake.net>.
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Refactored

 - Global constants fully shared among components through `sflc_constants.h`.



## [0.4.4] - 2023-12-31

### Fixed

 - Fixed a memory allocation error on large devices due to the use of a function not meant for allocating large amount of memory.
 
### Changed

 - Variables `shuffled_psi_array` and `shuffled_psi_ctr` renamed to `prmslices` and `prmslices_octr` respectively, to be more consistent with reference paper.
 - Minor edits to README.md.
 

## [0.4.3] - 2023-11-29

### Fixed

 - Fixed compile error on kernel version >= 6.4 due to changed behavior of DEFINE_SEMAPHORE macro.
 - Fixed minor cosmetic bug on some benchmark scripts.
 
 
## [0.4.2] - 2023-11-28

### Fixed

 - Fixed persistent slice allocation ambiguity after a volume corruption by allocating fresh slices for the corrupted volume. This is done in order to help external recovery tools (e.g. RAID).
 - Fixed a bug that made the last PSI in the shuffled slice array unassignable.
 - Fixed a missed deallocation problem which caused a kernel bug on volume close after some I/O errors.
 - Fixed a buggy swap procedure which made the permutation of PSIs not completely random.

### Changed

 - All schematics and references now consistently map array indices of size N from 0 to N-1 rather than from 1 to N.
 

## [0.4.1] - 2023-07-30

### Fixed

 - Fixed and improved benchmark scripts.
 - Fixed mistake in drawing of `doc/headers.png` and `doc/headers.svg`.


## [0.4.0] - 2023-07-24

### Added
 - Benchmark suite with testing tools for Shufflecake, LUKS, and VeraCrypt.
 - `changepwd` action to change an existing password.
 - `checkpwd` action to check if a given password is correct.
 - Improved documentation in `README.md` on using `init` non-interactively.
 - `doc` section which for now includes figure of Shufflecake header structure.

### Refactored

 - Implemented reference slice allocation algorithm with much faster performance.
 
### Fixed

 - Fixed a bug that made `--skip-randfill` option not work.
 - Fixed a bug that made action `close` not work.
 
### Changed

 - BREAKING CHANGE: slightly modified header field format, removing redundant MAC field and making it adherent to documentation.
 - Action `init` now reads password from secure interface (not echoing characters, etc).
 - Updated instructions in `SECURITY.md`.


## [0.3.1] - 2023-07-15

### Added

- Interactive test for Argon2id KDF.
- Added in `README.md` a description for a manual procedure for increasing volume resistance to corruption using RAID.
- Fully functional badges images in `README.md`.

### Fixed

 - BREAKING CHANGE: fixed parameters for Argon2id KDF, which were previously set too high, resulting in slow opening for non-recent devices. Unfortunately this breaks header format compatibility with v0.3.0, so please treat v0.3.0 as bugged.


## [0.3.0] - 2023-07-11

### Refactored

 - Unified repositories of kernel module and userland tool into a single one. Makefile, docs, etc unified accordingly.
 - Adopting SemVer as of release v0.3.0.
 - Program version now defined within `sflc_constants.h`.
 - CLI built with argp.h and following GNU behavior.
 
### Fixed

 - Test routines available with `make install`, for now limited to low-level crypto operations.
 
### Changed

 - License changed from GPLv3+ to GPLv2+.
 - Switched from Scrypt to Argon2id as KDF.
 - Removed (or made optional) most interaction from the action commands. Now a block device path is a mandatory argument for all actions. Moreover, some options can be passed by command line rather than asked interactively.
 - One global salt for deriving keys for all headers in order to not slow down too much opening. This allows us to avoid a difficult choice between insecurity against brute-force attacks and unacceptably slow opening time.
- Added a 1-block offset to accommodate for header format change.


## [0.2.0] - 2023-04-17

### Added

- Automatic procedural naming of open volumes.
- Support larger volumes with headers of variable size.
- Add interactive option to skip random filling during init.

### Fixed

- Compile correctly on Linux kernel 6.1.

### Changed

- Switch from `libsodium` to `libgcrypt`.
- Change header format.
- Change syntax of commands.
- Switch to Scrypt KDF.

### Removed

- Removed flag `--no-randfill`.


## [0.1.0] - 2022-11-10

This is the first release of `shufflecake-c`

