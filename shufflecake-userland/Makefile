#  Copyright The Shufflecake Project Authors (2022)
#  Copyright The Shufflecake Project Contributors (2022)
#  Copyright Contributors to the The Shufflecake Project.
  
#  See the AUTHORS file at the top-level directory of this distribution and at
#  <https://www.shufflecake.net/permalinks/shufflecake-c/AUTHORS>
  
#  This file is part of the program shufflecake-c, which is part of the Shufflecake 
#  Project. Shufflecake is a plausible deniability (hidden storage) layer for 
#  Linux. See <https://www.shufflecake.net>.
  
#  This program is free software: you can redistribute it and/or modify it 
#  under the terms of the GNU General Public License as published by the Free 
#  Software Foundation, either version 2 of the License, or (at your option) 
#  any later version. This program is distributed in the hope that it will be 
#  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
#  Public License for more details. You should have received a copy of the 
#  GNU General Public License along with this program. 
#  If not, see <https://www.gnu.org/licenses/>.

#############################################################################
# Makefile with dependency auto-generation, taken and adapted from
# https://make.mad-scientist.net/papers/advanced-auto-dependency-generation/
#############################################################################

# Output dirs for binaries
BIN_DIR := bin
PROJ_OUT_DIR := $(BIN_DIR)/proj_build
TEST_OUT_DIR := $(BIN_DIR)/test_build
# Output dirs for dependency files
PROJ_DEP_DIR := $(BIN_DIR)/.proj_deps
TEST_DEP_DIR := $(BIN_DIR)/.test_deps

# Include directories for compilation
INCLUDE := include test

# Use gcc
CC := gcc
# All warnings, add includes (other options may be supplied on command line)
override CFLAGS += -Wall $(addprefix -I,$(INCLUDE))
# Flags for dependency file auto-generation (deferred evaluation)
PROJ_DEPFLAGS = -MT $@ -MMD -MP -MF $(PROJ_DEP_DIR)/$*.d
TEST_DEPFLAGS = -MT $@ -MMD -MP -MF $(TEST_DEP_DIR)/$*.d
# Linker flags
LDFLAGS := -lgcrypt -ldevmapper

# The variables PROJ_SRCS (and PROJ_ROOT) and TEST_SRCS (and TEST_ROOT) are defined in this Makefile
include Makefile.sources
# Create the three lists of object files
PROJ_OBJS := $(PROJ_SRCS:$(PROJ_ROOT)/%.c=$(PROJ_OUT_DIR)/%.o)
TEST_OBJS := $(TEST_SRCS:$(TEST_ROOT)/%.c=$(TEST_OUT_DIR)/%.o)
PROJ_OBJS_NO_MAIN := $(filter-out $(PROJ_OUT_DIR)/main.o,$(PROJ_OBJS))
# Create the two lists of dependency files
PROJ_DEPS := $(PROJ_SRCS:$(PROJ_ROOT)/%.c=$(PROJ_DEP_DIR)/%.d)
TEST_DEPS := $(TEST_SRCS:$(TEST_ROOT)/%.c=$(TEST_DEP_DIR)/%.d)
# Put them together
DEPS := $(PROJ_DEPS) $(TEST_DEPS)

# All directories to be created if non-existing (sort to remove duplicates)
DIRS := $(sort $(dir $(BIN_DIR) $(PROJ_OBJS) $(TEST_OBJS) $(PROJ_DEPS) $(TEST_DEPS)))

# The target binaries
MAIN_BIN := $(PROJ_OUT_DIR)/shufflecake
TEST_BIN := $(TEST_OUT_DIR)/tests
# Their symlink
MAIN_LINK := shufflecake
TEST_LINK := tests



####
#### RULES
####


.PHONY: main
main: $(MAIN_BIN)

.PHONY: test
test: $(TEST_BIN)

.PHONY: link_msg
link_msg:
	@echo "Linking object files"

.PHONY: compile_msg
compile_msg:
	@echo "Compiling source files"

# Link project object files
$(MAIN_BIN): $(PROJ_OBJS) | link_msg
	@echo "\t---> $@"
	@$(CC) $^ -o $@ $(LDFLAGS)
	@rm -f $(MAIN_LINK)
	@ln -s $@ $(MAIN_LINK)

# Link test object files
$(TEST_BIN): $(PROJ_OBJS_NO_MAIN) $(TEST_OBJS) | link_msg
	@echo "\t---> $@"
	@$(CC) $^ -o $@ $(LDFLAGS)
	@rm -f $(TEST_LINK)
	@ln -s $@ $(TEST_LINK)
	@echo "Done, launching tests"
	@./$(TEST_LINK)

# Cancel implicit rule
%.o : %.c

# Build project object file
$(PROJ_OUT_DIR)/%.o : $(PROJ_ROOT)/%.c $(PROJ_DEP_DIR)/%.d | $(DIRS) compile_msg
	@echo "\t---> $@"
	@$(CC) $(PROJ_DEPFLAGS) $(CFLAGS) -c -o $@ $<

# Build test object file
$(TEST_OUT_DIR)/%.o : $(TEST_ROOT)/%.c $(TEST_DEP_DIR)/%.d | $(DIRS) compile_msg
	@echo "\t---> $@"
	@$(CC) $(TEST_DEPFLAGS) $(CFLAGS) -c -o $@ $<

# Create needed directories
$(DIRS):
	@mkdir -p $@

.PHONY: clean
clean:
	rm -rf $(PROJ_OUT_DIR) $(TEST_OUT_DIR) $(PROJ_DEP_DIR) $(TEST_DEP_DIR)
	rm -rf $(BIN_DIR)
	@rm -f $(MAIN_LINK) $(TEST_LINK)

$(DEPS):
include $(wildcard $(DEPS))
