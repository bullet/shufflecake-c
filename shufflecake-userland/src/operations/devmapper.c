/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, either version 2 of the License, or (at your option)
 *  any later version. This program is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 *  Public License for more details. You should have received a copy of the
 *  GNU General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "header.h"
#include "operations.h"
#include "utils/sflc.h"
#include "utils/crypto.h"
#include "utils/file.h"
#include "utils/string.h"
#include "utils/dm.h"
#include "utils/log.h"


/*****************************************************
 *          PUBLIC FUNCTIONS DEFINITIONS             *
 *****************************************************/

/**
 * Build parameter list for ctor in dm_sflc, and send DM ioctl to create
 * virtual block device.
 *
 * @param bdev_path The path to the underlying device
 * @param dev_id The ID of the underlying block device
 * @param vol_idx The index of the volume within the device
 * @param vmb Volume metadata
 *
 * @return Error code, 0 on success
 */
int sflc_ops_openVolume(char *bdev_path, size_t dev_id, size_t vol_idx, sflc_Vmb *vmb)
{
	char label[SFLC_BIGBUFSIZE];
	char *hex_key;
	char params[SFLC_BIGBUFSIZE];
	uint64_t num_sectors;
	int err;

	/* Build volume label */
	sprintf(label, "sflc_%lu_%lu", dev_id, vol_idx);

	/* Get the hex version of the volume's data section key */
	hex_key = sflc_toHex(vmb->volume_key, SFLC_CRYPTO_KEYLEN);
	if (!hex_key) {
		sflc_log_error("Could not encode volume key to hexadecimal");
		err = ENOMEM;
		goto err_hexkey;
	}

	/* Get the number of logical 512-byte sectors composing the volume */
	num_sectors = ((uint64_t) vmb->nr_slices) * SFLC_BLOCKS_PER_LOG_SLICE * SFLC_SECTOR_SCALE;

	/* Build param list */
	sprintf(params, "%s %lu %lu %s", bdev_path, vol_idx, vmb->nr_slices, hex_key);

	/* Issue ioctl */
	err = sflc_dm_create(label, num_sectors, params);
	if (err) {
		sflc_log_error("Could not issue ioctl CREATE command to device mapper; error %d", err);
		goto err_dmcreate;
	}
	err = 0;


err_dmcreate:
	free(hex_key);
err_hexkey:
	return err;
}


/**
 * Close the volume by issuing the appropriate ioctl to the DM.
 *
 * @param label The only needed parameter: the ID of the volume.
 *
 * @return Error code, 0 on success
 */
int sflc_ops_closeVolume(char *label)
{
	/* Issue ioctl */
	return sflc_dm_destroy(label);
}
