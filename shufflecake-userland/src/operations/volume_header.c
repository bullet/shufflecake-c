/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, either version 2 of the License, or (at your option)
 *  any later version. This program is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 *  Public License for more details. You should have received a copy of the
 *  GNU General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "header.h"
#include "header.h"
#include "operations.h"
#include "utils/sflc.h"
#include "utils/disk.h"
#include "utils/crypto.h"
#include "utils/log.h"


/*****************************************************
 *          PUBLIC FUNCTIONS DEFINITIONS             *
 *****************************************************/

/**
 * Writes a volume header (VMB+PM) on-disk.
 *
 * @param bdev_path The underlying block device to write the volume header
 * @param vmb_key The key to encrypt the VMB
 * @param vmb The VMB to encrypt
 * @param vol_idx The index of the volume within the device
 *
 * @return Error code, 0 on success
 */
int sflc_ops_writeVolumeHeader(char *bdev_path, char *vmb_key, sflc_Vmb *vmb, size_t vol_idx)
{
	char enc_vmb[SFLC_SECTOR_SIZE];
	sflc_EncPosMap epm;
	uint64_t sector;
	int err;

	// Encrypt VMB
	err = sflc_vmb_seal(vmb, vmb_key, enc_vmb);
	if (err) {
		sflc_log_error("Could not seal VMB; error %d", err);
		goto out;
	}

	// Write it to disk
	sector = sflc_vmbPosition(vol_idx, vmb->nr_slices);
	err = sflc_disk_writeSector(bdev_path, sector, enc_vmb);
	if (err) {
		sflc_log_error("Could not write VMB to disk; error %d", err);
		goto out;
	}
	sector += 1;

	// Create encrypted empty position map
	err = sflc_epm_create(vmb->nr_slices, vmb->volume_key, &epm);
	if (err) {
		sflc_log_error("Could not create encrypted empty position map; error %d", err);
		goto out;
	}

	// Loop over PMB arrays to write them to disk
	int i;
	for (i = 0; i < epm.nr_arrays; i++) {
		char *iv_block = epm.iv_blocks[i];
		char *pmb_array = epm.pmb_arrays[i];
		size_t nr_pmbs = ((i == epm.nr_arrays-1) ? epm.nr_last_pmbs : SFLC_BLOCKS_PER_LOG_SLICE);

		// First write the IV block
		err = sflc_disk_writeSector(bdev_path, sector, iv_block);
		if (err) {
			sflc_log_error("Could not write IV block to disk; error %d", err);
			goto out;
		}
		sector += 1;

		// Then the whole PMB array
		err = sflc_disk_writeManySectors(bdev_path, sector, pmb_array, nr_pmbs);
		if (err) {
			sflc_log_error("Could not write PMB array to disk; error %d", err);
			goto out;
		}
		sector += nr_pmbs;

		// Free them both
		free(iv_block);
		free(pmb_array);
	}

	// Free containers
	free(epm.iv_blocks);
	free(epm.pmb_arrays);


out:
	return err;
}


/**
 * Reads a VMB from disk and unlocks it
 *
 * @param bdev_path The underlying block device
 * @param vmb_key The key to decrypt the VMB
 * @param nr_slices The number of slices in the device
 * @param vol_idx The index of the volume within the device
 *
 * @output vmb The decrypted VMB
 *
 * @return Error code, 0 on success
 */
int sflc_ops_readVmb(char *bdev_path, char *vmb_key, size_t nr_slices, size_t vol_idx, sflc_Vmb *vmb)
{
	char enc_vmb[SFLC_SECTOR_SIZE];
	uint64_t sector;
	int err;

	/* Read encrypted VMB from disk */
	sector = sflc_vmbPosition(vol_idx, nr_slices);
	err = sflc_disk_readSector(bdev_path, sector, enc_vmb);
	if (err) {
		sflc_log_error("Could not read VMB from disk; error %d", err);
		return err;
	}

	/* Unseal it */
	err = sflc_vmb_unseal(enc_vmb, vmb_key, vmb);
	if (err) {
		sflc_log_error("Could not unseal VMB; error %d", err);
		return err;
	}

	/* Compare the number of slices */
	if (nr_slices != vmb->nr_slices) {
		sflc_log_error("Incompatible header size: the device size was different when the volumes"
				"were created. Did you resize the device %s since last time?", bdev_path);
		return EINVAL;
	}

	return 0;
}
