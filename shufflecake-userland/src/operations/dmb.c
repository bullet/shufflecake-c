/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, either version 2 of the License, or (at your option)
 *  any later version. This program is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 *  Public License for more details. You should have received a copy of the
 *  GNU General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "header.h"
#include "operations.h"
#include "utils/sflc.h"
#include "utils/disk.h"
#include "utils/crypto.h"
#include "utils/log.h"


/*****************************************************
 *          PUBLIC FUNCTIONS DEFINITIONS             *
 *****************************************************/

/**
 *  Encrypts and writes the DMB to disk.
 *
 *  @param bdev_path The path to the underlying block device
 *  @param pwds The array of passwords
 *  @param pwd_lens Their lengths
 *  @param dmb->nr_vols
 *
 *
 *  @return Error code, 0 on success
 */
int sflc_ops_writeDmb(char *bdev_path, char **pwds, size_t *pwd_lens, sflc_Dmb *dmb)
{
	/* On-disk DMB */
	char enc_dmb[SFLC_SECTOR_SIZE];
	/* Error code */
	int err;

	/* Sanity check */
	if (dmb->nr_vols > SFLC_DEV_MAX_VOLUMES) {
		sflc_log_error("Cannot create %lu volumes, too many!", dmb->nr_vols);
		return EINVAL;
	}

	/* Seal DMB */
	err = sflc_dmb_seal(dmb, pwds, pwd_lens, enc_dmb);
	if (err) {
		sflc_log_error("Coul dnot seal DMB; error %d", err);
		return err;
	}

	/* Write it to disk (at sector 0) */
	err = sflc_disk_writeSector(bdev_path, 0, enc_dmb);
	if (err) {
		sflc_log_error("Could not write DMB to disk; error %d", err);
		return err;
	}

	return 0;
}


/**
 *  Reads the DMB from disk and outputs the unlocked VMB key.
 *
 *  @param bdev_path The path to the underlying block device
 *  @param pwd The user-provided password
 *  @param pwd_len Its length
 *
 *  @output dmb_cell->vmb_key The unlocked VMB key
 *  @output dmb_cell->vol_idx Its index (>= SFLC_DEV_MAX_VOLUMES if none could be unlocked)
 *
 *  @return Error code, 0 on success
 */
int sflc_ops_readDmb(char *bdev_path, char *pwd, size_t pwd_len, sflc_DmbCell *dmb)
{
	char enc_dmb[SFLC_SECTOR_SIZE];
	int err;

	/* Read DMB from disk (at sector 0) */
	err = sflc_disk_readSector(bdev_path, 0, enc_dmb);
	if (err) {
		sflc_log_error("Could not read DMB from disk; error %d", err);
		return err;
	}

	/* Unseal it */
	err = sflc_dmb_unseal(enc_dmb, pwd, pwd_len, dmb);
	if (err) {
		sflc_log_error("Could not unseal DMB; error %d", err);
		return err;
	}

	return 0;
}

/**
 * Reads the DMB from disk, changes the relevant DMB cell, and writes it back.
 *
 * @param bdev_path The path to the underlying block device
 * @param dmb_cell The index and vmb_key of the DMB cell to re-encrypt
 * @param new_pwd The new password of the DMB cell to re-encrypt
 * @param new_pwd_len Its length
 *
 * @return Error code, 0 on success
 */
int sflc_ops_rewriteDmbCell(char *bdev_path, sflc_DmbCell *dmb_cell, char *new_pwd, size_t new_pwd_len)
{
	char enc_dmb[SFLC_SECTOR_SIZE];
	int err;

	/* Sanity check */
	if (dmb_cell->vol_idx >= SFLC_DEV_MAX_VOLUMES) {
		sflc_log_error("Cannot re-encrypt DMB cell %lu: out of bounds!", dmb_cell->vol_idx);
		return EINVAL;
	}

	/* Read DMB from disk (at sector 0) */
	err = sflc_disk_readSector(bdev_path, 0, enc_dmb);
	if (err) {
		sflc_log_error("Could not read DMB from disk; error %d", err);
		return err;
	}

	/* Update the relevant cell */
	err = sflc_dmb_setCell(enc_dmb, dmb_cell, new_pwd, new_pwd_len);
	if (err) {
		sflc_log_error("Could not update DMB cell; error %d", err);
		return err;
	}

	/* Write it to disk (at sector 0) */
	err = sflc_disk_writeSector(bdev_path, 0, enc_dmb);
	if (err) {
		sflc_log_error("Could not write DMB to disk; error %d", err);
		return err;
	}

	return 0;
}


