/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, either version 2 of the License, or (at your option)
 *  any later version. This program is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 *  Public License for more details. You should have received a copy of the
 *  GNU General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h>	// Network byte order

#include "header.h"
#include "utils/crypto.h"
#include "utils/string.h"
#include "utils/log.h"


/*****************************************************
 *          PRIVATE FUNCTIONS PROTOTYPES             *
 *****************************************************/

/* Serialise the VMB before encrypting it */
static void _serialiseVmb(sflc_Vmb *vmb, char *clear_vmb);

/* Deserialise the VMB after decrypting it */
static int _deserialiseVmb(char *clear_vmb, sflc_Vmb *vmb);


/*****************************************************
 *          PUBLIC FUNCTIONS DEFINITIONS             *
 *****************************************************/

/**
 * Builds the on-disk master block (indistinguishable from random).
 *
 * @param vmb The useful information stored in this volume master block
 * @param vmb_key The key encrypting the VMB
 * @param disk_block The 4096-byte buffer that will contain the random-looking
 *  bytes to be written on-disk
 *
 * @return The error code, 0 on success
 */
int sflc_vmb_seal(sflc_Vmb *vmb, char *vmb_key, char *disk_block)
{
	// Pointers inside the block
	char *iv = disk_block;
	char *enc_vmb = iv + SFLC_AESCTR_IVLEN;
	// Serialised VMB (dynamically allocated), to be encrypted
	char *clear_vmb;
	// Error code
	int err;

	/* Allocate large buffer on the heap */
	clear_vmb = malloc(SFLC_CLEAR_VMB_LEN);
	if (!clear_vmb) {
		sflc_log_error("Could not allocate %d bytes for VMB cleartext", SFLC_CLEAR_VMB_LEN);
		err = ENOMEM;
		goto bad_clear_alloc;
	}
	sflc_log_debug("Successfully allocated %d bytes for VMB cleartext", SFLC_CLEAR_VMB_LEN);

	/* Serialise the struct */
	_serialiseVmb(vmb, clear_vmb);
	sflc_log_debug("Serialised VMB struct");

	/* Sample VMB IV */
	err = sflc_rand_getWeakBytes(iv, SFLC_AESGCM_PADDED_IVLEN);
	if (err) {
		sflc_log_error("Could not sample VMB IV: error %d", err);
		goto bad_sample_iv;
	}
	sflc_log_debug("Successfully sampled VMB IV");

	/* Encrypt the VMB */
	err = sflc_aes256ctr_encrypt(vmb_key, clear_vmb, SFLC_CLEAR_VMB_LEN, iv, enc_vmb);
	if (err) {
		sflc_log_error("Could not encrypt VMB: error %d", err);
		goto bad_encrypt;
	}
	sflc_log_debug("Successfully encrypted VMB");

	// No prob
	err = 0;


bad_encrypt:
bad_sample_iv:
	/* Always wipe and free the cleartext VMB, even on success */
	memset(clear_vmb, 0, SFLC_CLEAR_VMB_LEN);
	free(clear_vmb);
bad_clear_alloc:
	return err;
}


/**
 * Decrypt the VMB payload using the VMB key.
 *
 * @param disk_block The content of the on-disk encrypted VMB
 * @param vmb_key The proposed VMB key to unseal its payload
 * @param vmb A pointer to the output struct that will contain all the VMB fields
 *
 * @return An error code, 0 on success
 */
int sflc_vmb_unseal(char *disk_block, char *vmb_key, sflc_Vmb *vmb)
{
	// Pointers inside the block
	char *iv = disk_block;
	char *enc_vmb = iv + SFLC_AESCTR_IVLEN;
	// Decrypted VMB (dynamically allocated), to be deserialised
	char *clear_vmb;
	// Error code
	int err;

	/* Allocate large buffer on the heap */
	clear_vmb = malloc(SFLC_CLEAR_VMB_LEN);
	if (!clear_vmb) {
		sflc_log_error("Could not allocate %d bytes for VMB cleartext", SFLC_CLEAR_VMB_LEN);
		err = ENOMEM;
		goto bad_clear_alloc;
	}
	sflc_log_debug("Successfully allocated %d bytes for VMB cleartext", SFLC_CLEAR_VMB_LEN);

	/* Decrypt the VMB */
	err = sflc_aes256ctr_decrypt(vmb_key, enc_vmb, SFLC_CLEAR_VMB_LEN, iv, clear_vmb);
	if (err) {
		sflc_log_error("Error while decrypting VMB: error %d", err);
		goto bad_decrypt;
	}
	sflc_log_debug("Successfully decrypted VMB");

	/* Deserialise the struct */
	err = _deserialiseVmb(clear_vmb, vmb);
	if (err) {
		sflc_log_error("Error while deserialising VMB: error %d", err);
		goto bad_deserialise;
	}
	sflc_log_debug("Deserialised VMB struct");

	// No prob
	err = 0;


bad_deserialise:
bad_decrypt:
	/* Always wipe and free the VMB cleartext, even on success */
	memset(clear_vmb, 0, SFLC_CLEAR_VMB_LEN);
	free(clear_vmb);
bad_clear_alloc:
	return err;
}


/*****************************************************
 *          PRIVATE FUNCTIONS PROTOTYPES             *
 *****************************************************/

/* Serialise the payload before encrypting it */
static void _serialiseVmb(sflc_Vmb *vmb, char *clear_vmb)
{
	// Pointers inside the VMB
	char *p_vol_key = clear_vmb;
	char *p_prev_vmb_key = p_vol_key + SFLC_CRYPTO_KEYLEN;
	char *p_nr_slices = p_prev_vmb_key + SFLC_CRYPTO_KEYLEN;

	/* Copy the volume key */
	memcpy(p_vol_key, vmb->volume_key, SFLC_CRYPTO_KEYLEN);

	/* Copy the previous volume's VMB key */
	memcpy(p_prev_vmb_key, vmb->prev_vmb_key, SFLC_CRYPTO_KEYLEN);

	/* Write the number of slices (network byte order) */
	*((uint32_t *) p_nr_slices) = htonl(vmb->nr_slices);

	// Leave the rest uninitialised

	return;
}


/* Deserialise the VMB  after decrypting it */
static int _deserialiseVmb(char *clear_vmb, sflc_Vmb *vmb)
{
	// Pointers inside the VMB
	char *p_vol_key = clear_vmb;
	char *p_prev_vmb_key = p_vol_key + SFLC_CRYPTO_KEYLEN;
	char *p_nr_slices = p_prev_vmb_key + SFLC_CRYPTO_KEYLEN;

	/* Copy the volume key */
	memcpy(vmb->volume_key, p_vol_key, SFLC_CRYPTO_KEYLEN);

	/* Copy the previous volume's VMB key */
	memcpy(vmb->prev_vmb_key, p_prev_vmb_key, SFLC_CRYPTO_KEYLEN);

	/* Read number of slices (network byte order) */
	vmb->nr_slices = ntohl( *((uint32_t *) p_nr_slices) );

	// Ignore the rest

	return 0;
}

