/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, either version 2 of the License, or (at your option)
 *  any later version. This program is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 *  Public License for more details. You should have received a copy of the
 *  GNU General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "header.h"
#include "utils/crypto.h"
#include "utils/log.h"


/*****************************************************
 *          PRIVATE FUNCTIONS PROTOTYPES             *
 *****************************************************/

/* AES-GCM-encrypt a VMB key with the KDF-generated key */
static int _encryptVmbKeyWithPwd(char *salt, char *pwd, size_t pwd_len, char *vmb_key, char *dmb_cell);

/* AES-GCM-decrypt a VMB key with the KDF-generated key */
static int _decryptVmbKeyWithPwd(char *dmb_cell, char *kek, char *vmb_key, bool *match);


/*****************************************************
 *          PUBLIC FUNCTIONS DEFINITIONS             *
 *****************************************************/

/**
 * Builds the on-disk device master block (indistinguishable from random).
 *
 * @param dmb The list of VMB keys
 * @param pwds The passwords of the volumes
 * @param pwd_lens Their lengths
 * @param disk_block The 4096-byte buffer that will contain the random-looking
 *  bytes to be written on-disk
 *
 * @return The error code, 0 on success
 */
int sflc_dmb_seal(sflc_Dmb *dmb, char **pwds, size_t *pwd_lens, char *disk_block)
{
	char *salt;
	int err;

	/* Sanity check */
	if (dmb->nr_vols > SFLC_DEV_MAX_VOLUMES) {
		sflc_log_error("Cannot create DMB with %lu volumes, too many!", dmb->nr_vols);
		return EINVAL;
	}

	/* Randomise whole block */
	err = sflc_rand_getWeakBytes(disk_block, SFLC_SECTOR_SIZE);
	if (err) {
		sflc_log_error("Could not randomise DMB; error %d", err);
		return err;
	}

	/* Assign salt */
	salt = disk_block;

	/* Loop over all VMB keys to encrypt them */
	size_t i;
	for (i = 0; i < dmb->nr_vols; i++) {
		char *dmb_cell = (salt + SFLC_ARGON_SALTLEN) + (i * SFLC_DMB_CELL_SIZE);

		/* Encrypt it */
		err = _encryptVmbKeyWithPwd(salt, pwds[i], pwd_lens[i], dmb->vmb_keys[i], dmb_cell);
		if (err) {
			sflc_log_error("Could not encrypt VMB key number %lu; error %d", i, err);
			return err;
		}
	}

	return 0;
}


/**
 * "Decrypt" a single VMB key from the on-disk DMB, using its password (perform the KDF).
 *
 *  @param disk_block The on-disk sealed DMB
 *  @param pwd The password locking the VMB key
 *  @param pwd_len Its length
 *
 *  @output dmb_cell->vmb_key The unlocked VMB key
 *  @output dmb_cell->vol_idx Its index (>= SFLC_DEV_MAX_VOLUMES if none could be unlocked)
 *
 *  @return Error code, 0 on success
 */
int sflc_dmb_unseal(char *disk_block, char *pwd, size_t pwd_len, sflc_DmbCell *dmb_cell)
{
	// KDF salt
	char *salt;
	// The KDF-derived key
	char kek[SFLC_CRYPTO_KEYLEN];
	// The unlocked VMB key
	char vmb_key[SFLC_CRYPTO_KEYLEN];
	// Error code
	int err;

	/* Derive KEK once and for all */
	salt = disk_block;
	err = sflc_argon2id_derive(pwd, pwd_len, salt, kek);
	if (err) {
		sflc_log_error("Could not perform KDF: error %d", err);
		goto bad_kdf;
	}
	sflc_log_debug("Successfully derived key-encryption-key with KDF");

	/* Init dmb->vol_idx to invalid */
	dmb_cell->vol_idx = SFLC_DEV_MAX_VOLUMES;
	/* Try all DMB cells */
	size_t i;
	for (i = 0; i < SFLC_DEV_MAX_VOLUMES; i++) {
		char *enc_dmb_cell = (salt + SFLC_ARGON_SALTLEN) + (i * SFLC_DMB_CELL_SIZE);
		bool match;

		/* Try to decrypt this one */
		err = _decryptVmbKeyWithPwd(enc_dmb_cell, kek, vmb_key, &match);
		if (err) {
			sflc_log_error("Error decrypting DMB cell number %lu; error %d", i, err);
			goto bad_decrypt;
		}

		/* If MAC matched, mark it, but don't break from the loop (timing attacks) */
		if (match) {
			sflc_log_debug("The provided password unlocks volume %lu", i);
			dmb_cell->vol_idx = i;
			memcpy(dmb_cell->vmb_key, vmb_key, SFLC_CRYPTO_KEYLEN);
		}
	}

	// No prob
	err = 0;


bad_decrypt:
bad_kdf:
	/* Always wipe the key from memory, even on success */
	memset(kek, 0, SFLC_CRYPTO_KEYLEN);
	return err;
}

/**
 * Re-encrypt the content of the specified DMB cell.
 *
 * @param disk_block The on-disk sealed DMB
 * @param dmb_cell The DMB cell to re-encrypt
 * @param pwd The new password
 * @param pwd_len The password's length
 *
 * @return Error code, 0 on success
 */
int sflc_dmb_setCell(char *disk_block, sflc_DmbCell *dmb_cell, char *pwd, size_t pwd_len)
{
	char *salt;
	char *enc_dmb_cell;
	int err;

	/* Sanity check */
	if (dmb_cell->vol_idx >= SFLC_DEV_MAX_VOLUMES) {
		sflc_log_error("Cannot set DMB cell %lu: out of bounds!", dmb_cell->vol_idx);
		return EINVAL;
	}

	/* Pointers inside DMB */
	salt = disk_block;
	enc_dmb_cell = (salt + SFLC_ARGON_SALTLEN) + (dmb_cell->vol_idx * SFLC_DMB_CELL_SIZE);
	/* Encrypt with KDF-derived key */
	err = _encryptVmbKeyWithPwd(salt, pwd, pwd_len, dmb_cell->vmb_key, enc_dmb_cell);
	if (err) {
		sflc_log_error("Could not re-encrypt VMB key number %lu; error %d", dmb_cell->vol_idx, err);
		return err;
	}
	sflc_log_debug("Successfully re-encrypted VMB key number %lu", dmb_cell->vol_idx);

	return 0;
}


/*****************************************************
 *          PRIVATE FUNCTIONS PROTOTYPES             *
 *****************************************************/

/* AES-GCM-encrypt a VMB key with the KDF-generated key */
static int _encryptVmbKeyWithPwd(char *salt, char *pwd, size_t pwd_len, char *vmb_key, char *dmb_cell)
{
	// Pointers inside the block
	char *iv = dmb_cell;
	char *enc_vmb_key = iv + SFLC_AESGCM_PADDED_IVLEN;
	char *mac = enc_vmb_key + SFLC_CRYPTO_KEYLEN;
	// Key-encryption-key derived from KDF
	char kek[SFLC_CRYPTO_KEYLEN];
	// Error code
	int err;

	/* Derive KEK */
	err = sflc_argon2id_derive(pwd, pwd_len, salt, kek);
	if (err) {
		sflc_log_error("Could not perform KDF: error %d", err);
		goto bad_kdf;
	}
	sflc_log_debug("Successfully derived key-encryption-key with KDF");

	/* Sample VMB_IV */
	err = sflc_rand_getWeakBytes(iv, SFLC_AESGCM_PADDED_IVLEN);
	if (err) {
		sflc_log_error("Could not sample prologue IV: error %d", err);
		goto bad_sample_iv;
	}
	sflc_log_debug("Successfully sampled prologue IV");

	/* Encrypt the VMB key */
	err = sflc_aes256gcm_encrypt(kek, vmb_key, SFLC_CRYPTO_KEYLEN, iv, enc_vmb_key, mac);
	if (err) {
		sflc_log_error("Could not encrypt the VMB key: error %d", err);
		goto bad_encrypt;
	}
	sflc_log_debug("Successfully encrypted VMB key with key-encryption-key");

	// No prob
	err = 0;


bad_encrypt:
bad_sample_iv:
bad_kdf:
	/* Always wipe the key from memory, even on success */
	memset(kek, 0, SFLC_CRYPTO_KEYLEN);
	return err;
}

/* AES-GCM-decrypt a VMB key with the KDF-generated key */
static int _decryptVmbKeyWithPwd(char *dmb_cell, char *kek, char *vmb_key, bool *match)
{
	// Pointers inside the block
	char *iv = dmb_cell;
	char *enc_vmb_key = iv + SFLC_AESGCM_PADDED_IVLEN;
	char *mac = enc_vmb_key + SFLC_CRYPTO_KEYLEN;
	// Error code
	int err;

	/* Decrypt the VMB key */
	err = sflc_aes256gcm_decrypt(kek, enc_vmb_key, SFLC_CRYPTO_KEYLEN, mac, iv, vmb_key, match);
	if (err) {
		sflc_log_error("Error while decrypting VMB key: error %d", err);
		return err;
	}
	sflc_log_debug("Decrypted VMB key: MAC match = %d", *match);

	return 0;
}

