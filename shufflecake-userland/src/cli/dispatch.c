/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, either version 2 of the License, or (at your option)
 *  any later version. This program is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 *  Public License for more details. You should have received a copy of the
 *  GNU General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <string.h>
#include <argp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "cli.h"
#include "utils/sflc.h"
#include "utils/disk.h"
#include "utils/log.h"
#include "sflc_constants.h"


/*****************************************************
 *                    CONSTANTS                      *
 *****************************************************/

/* Used by argp to provide the automatic "-V" option */
const char *argp_program_version = SFLC_VERSION;
/* Used by argp to provide the automatic "--help" option */
const char *argp_program_bug_address = "<bugs@shufflecake.net>";

/* Signed integer values representing a handle for each option */
#define SFLC_OPT_NUMVOLS_KEY	'n'	// Positive and printable: also serves as short command-line option
#define SFLC_OPT_SKIPRAND_KEY	(-'r')	// Negative, because we don't want a short option available for this


/*****************************************************
 *                      TYPES                        *
 *****************************************************/

enum sflc_cli_action {
    SFLC_ACT_INIT,
    SFLC_ACT_OPEN,
    SFLC_ACT_CLOSE,
    SFLC_ACT_TESTPWD,
    SFLC_ACT_CHANGEPWD
};

struct sflc_cli_arguments {
    enum sflc_cli_action	act;
    char 			*block_device;
    int 			num_volumes;
    bool 			skip_randfill;
};


/*****************************************************
 *          PRIVATE FUNCTIONS PROTOTYPES             *
 *****************************************************/

static error_t _parseArgpKey(int key, char *arg, struct argp_state *state);


/*****************************************************
 *                PRIVATE VARIABLES                  *
 *****************************************************/

/* Doc strings */
static char args_doc[] = "ACTION <block_device>";
static char doc[] =
	"Shufflecake is a plausible deniability (hidden storage) layer for Linux.\n"
	"See official website at <https://shufflecake.net> for more info.\n"
	"Possible values for mandatory ACTION are:\n\n"

	"\tinit:\t\tInitialise a block device for Shufflecake use, formatting\n"
	"\t\t\theaders with provided passwords and overwriting with random\n"
	"\t\t\tdata. WARNING: THIS WILL ERASE THE CONTENT OF THE DEVICE.\n\n"

	"\topen:\t\tOpen a hierarchy of Shufflecake volumes within a device by\n"
	"\t\t\tasking a single user password. Virtual devices will appear\n"
	"\t\t\tin /dev/mapper. Notice: freshly created device must be\n"
	"\t\t\tuser-formatted and mounted in order to be used.\n\n"

	"\tclose:\t\tClose all open Shufflecake volumes supported by given\n"
	"\t\t\tdevice.\n\n"

	"\ttestpwd:\tTest whether a given password unlocks any volume within\n"
	"\t\t\tthe block device and, if so, show its index.\n\n"

	"\tchangepwd:\tChange the password unlocking a certain volume.\n\n"

	"Possible options are:";

/* Description of each option */
static struct argp_option options[] = {
    {"num-volumes", SFLC_OPT_NUMVOLS_KEY, "num", 0,
		    "Specify number of volumes to be created with `init'. Must be an integer between 1 and 15.", 0 }, // TODO: define MAX_VOLS instead of hardcoding 15
    {"skip-randfill", SFLC_OPT_SKIPRAND_KEY, 0, 0,
		    "Skip pre-overwriting block device with random data, only valid with `init'. Faster but less secure. Use only for debugging or testing."},
    {0}
};

/* Wrapper containing everything */
static struct argp argp = {options, _parseArgpKey, args_doc, doc};


/*****************************************************
 *          PUBLIC FUNCTIONS DEFINITIONS             *
 *****************************************************/

/*
 * The following is the main dispatch function called by the main to parse 
 * the arguments and dispatch to the right command.
 *
 * @param argc The number of command-line arguments supplied to the main
 * @param argv The arguments
 *
 * @return Error code, 0 on success
 */

int sflc_cli_dispatch(int argc, char **argv) {
    struct sflc_cli_arguments arguments;

    arguments.act = -1;
    arguments.block_device = NULL;
    arguments.num_volumes = 0;
    arguments.skip_randfill = false;

    /* Parse */
    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    /* Check options consistency */
    if (arguments.num_volumes && arguments.act != SFLC_ACT_INIT) {
        printf("Error: --num-volumes (-n) can only be combined with `init'.\n");
        return EINVAL;
    }
    /* Check options consistency */
    if (arguments.skip_randfill && arguments.act != SFLC_ACT_INIT) {
        printf("Error: --skip-randfill can only be combined with `init'.\n");
        return EINVAL;
    }
    /* Check that input is actually a block device */
    if (strncmp(arguments.block_device, "/dev/", 5) != 0 || !sflc_disk_isBlockDevice(arguments.block_device)) {
        printf("Error: '%s' is not a valid block device.\n", arguments.block_device);
        return EINVAL;
    }

	/* Dispatch to specific command */
	if (arguments.act == SFLC_ACT_INIT) {
		return sflc_cli_init(arguments.block_device, arguments.num_volumes, arguments.skip_randfill);
	}
	if (arguments.act == SFLC_ACT_OPEN) {
		return sflc_cli_open(arguments.block_device);
	}
	if (arguments.act == SFLC_ACT_CLOSE) {
		return sflc_cli_close(arguments.block_device);
	}
	if (arguments.act == SFLC_ACT_TESTPWD) {
		return sflc_cli_testPwd(arguments.block_device);
	}
	if (arguments.act == SFLC_ACT_CHANGEPWD) {
		return sflc_cli_changePwd(arguments.block_device);
	}

    printf("\n");

    return EINVAL;
}


/*****************************************************
 *          PRIVATE FUNCTIONS DEFINITIONS            *
 *****************************************************/

static error_t _parseArgpKey(int key, char *arg, struct argp_state *state) {
    struct sflc_cli_arguments *arguments = state->input;

    switch (key) {
    	/* We are parsing an argument (not an option) */
        case ARGP_KEY_ARG:
            /* We are parsing the command */
            if (state->arg_num == 0) {
                if (strcmp(arg, SFLC_CLI_INITACT) == 0) {
                    arguments->act = SFLC_ACT_INIT;
                } else if (strcmp(arg, SFLC_CLI_OPENACT) == 0) {
                    arguments->act = SFLC_ACT_OPEN;
                } else if (strcmp(arg, SFLC_CLI_CLOSEACT) == 0) {
                    arguments->act = SFLC_ACT_CLOSE;
                } else if (strcmp(arg, SFLC_CLI_TESTPWDACT) == 0) {
                    arguments->act = SFLC_ACT_TESTPWD;
                } else if (strcmp(arg, SFLC_CLI_CHANGEPWDACT) == 0) {
                    arguments->act = SFLC_ACT_CHANGEPWD;
                } else {
                    argp_error(state, "Invalid action. Please enter one and only one of: `%s', `%s', `%s', '%s', or '%s'.",
                		    SFLC_CLI_INITACT, SFLC_CLI_OPENACT, SFLC_CLI_CLOSEACT, SFLC_CLI_TESTPWDACT, SFLC_CLI_CHANGEPWDACT);
                }
            /* We are parsing the block device */
            } else if (state->arg_num == 1) {
                arguments->block_device = arg;
            /* Too many arguments */
            } else {
                argp_usage(state);
            }
            break;

        /* We are parsing an option */
	case SFLC_OPT_NUMVOLS_KEY:
        	arguments->num_volumes = atoi(arg);
		break;
	case SFLC_OPT_SKIPRAND_KEY:
		arguments->skip_randfill = true;
		break;

	/* End of arg list */
        case ARGP_KEY_END:
            if (state->arg_num < 2) {
                argp_usage(state);
            }
            break;

        /* Unrecognised key */
        default:
            return ARGP_ERR_UNKNOWN;
    }

    return 0;
}
