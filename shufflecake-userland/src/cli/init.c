/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, either version 2 of the License, or (at your option)
 *  any later version. This program is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 *  Public License for more details. You should have received a copy of the
 *  GNU General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include "cli.h"
#include "commands.h"
#include "utils/sflc.h"
#include "utils/input.h"
#include "utils/log.h"


/*****************************************************
 *          PUBLIC FUNCTIONS DEFINITIONS             *
 *****************************************************/

/*
 * Create volumes
 *
 * @return Error code, 0 on success
 */
int sflc_cli_init(char *block_device, int num_volumes, int skip_randfill)
{	// Requires: block_device is a correct block device path
	sflc_cmd_InitArgs args;
	char str_nrvols[SFLC_BIGBUFSIZE];
	char *pwds[SFLC_DEV_MAX_VOLUMES];
	size_t pwd_lens[SFLC_DEV_MAX_VOLUMES];
	int err;

	args.bdev_path = block_device;
	
	// Check if number of volumes was nonzero passed by command line already
	if (num_volumes) {
		args.nr_vols = num_volumes;
	} else {
		// If not, ask user for number of volumes
		printf("\nHow many volumes do you want to create (maximum is %d)? ", SFLC_DEV_MAX_VOLUMES);
		err = sflc_safeReadLine(str_nrvols, SFLC_BIGBUFSIZE);
		if (err) {
			sflc_log_error("Error: could not read number of volumes; error %d", err);
			return err;
		}
		/* Parse string */
		if (sscanf(str_nrvols, "%lu\n", &args.nr_vols) != 1) {
			sflc_log_error("Error: could not parse number of volumes");
			return EINVAL;
		}
	}
		
	/* Bounds check */
	if (args.nr_vols <= 0) {
		printf("Error: number of volumes must be a positive integer");
		return EINVAL;
	}
	if (args.nr_vols > SFLC_DEV_MAX_VOLUMES) {
		printf("Number of volumes too high, Shufflecake supports up to %d volumes on a single device", SFLC_DEV_MAX_VOLUMES);
		return EINVAL;
	}

	/* Collects the passwords */
	printf("\nNow you will be asked to insert the passwords for all the volumes you want to create, \nfrom "
			"volume 0 (the least secret) to volume %lu (the most secret).\n\n", args.nr_vols - 1);
	size_t i;
	for (i = 0; i < args.nr_vols; i++) {
		// Allocate pwd
		pwds[i] = malloc(SFLC_BIGBUFSIZE);

		/* Read it */
		printf("Choose password for volume %lu (must not be empty): ", i);
		err = sflc_safeReadLine(pwds[i], SFLC_BIGBUFSIZE);
		if (err) {
			sflc_log_error("Could not read password for volume %lu; error %d", i, err);
			return err;
		}

		/* You can trust the length of strings input this way */
		pwd_lens[i] = strlen(pwds[i]);
		/* Check non-empty */
		if (pwd_lens[i] == 0) {
			sflc_log_error("Password cannot be empty!");
			return EINVAL;
		}
	}
	/* Assign them */
	args.pwds = pwds;
	args.pwd_lens = pwd_lens;

	args.no_randfill = skip_randfill;
	
	/* Actually perform the command */
	return sflc_cmd_initVolumes(&args);
}
