/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, either version 2 of the License, or (at your option)
 *  any later version. This program is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 *  Public License for more details. You should have received a copy of the
 *  GNU General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <errno.h>
#include <stdlib.h>
#include <stdint.h>

#include "commands.h"
#include "operations.h"
#include "utils/sflc.h"
#include "utils/crypto.h"
#include "utils/disk.h"
#include "utils/file.h"
#include "utils/log.h"


/*****************************************************
 *          PRIVATE FUNCTIONS PROTOTYPES             *
 *****************************************************/

/* Read the next device ID in sysfs */
static int _getNextDevId(size_t *next_dev_id);


/*****************************************************
 *          PUBLIC FUNCTIONS DEFINITIONS             *
 *****************************************************/

/**
 * Open M volumes, from the first one down to the one whose pwd is provided.
 * Scans the DMB cells to find which one is unlocked by the provided pwd; then,
 * using the decrypted VMB key, unlocks the M-th VMB; then, iteratively using
 * the prev_vmb_key field, unlocks all the previous VMBs; then, using the
 * decrypted VMB keys, opens the volumes "in order" from 1 to M.
 *
 * @param args->bdev_path The underlying block device
 * @param args->pwd The password
 * @param args->pwd_len The password length
 *
 * @return Error code (also if no volume could be opened), 0 on success
 */
int sflc_cmd_openVolumes(sflc_cmd_OpenArgs *args)
{
	int64_t dev_size;
	size_t nr_slices;
	sflc_DmbCell dmb_cell;
	sflc_Vmb vmbs[SFLC_DEV_MAX_VOLUMES];
	size_t dev_id;
	int err;

	/* Get number of slices */
	dev_size = sflc_disk_getSize(args->bdev_path);
	if (dev_size < 0) {
		err = -dev_size;
		sflc_log_error("Could not read device size for %s; error %d", args->bdev_path, err);
		return err;
	}
	nr_slices = sflc_disk_maxSlices(dev_size);

	/* Find volume opened by the pwd */
	err = sflc_ops_readDmb(args->bdev_path, args->pwd, args->pwd_len, &dmb_cell);
	if (err) {
		sflc_log_error("Could not read DMB; error %d", err);
		return err;
	}
	/* Was there one? */
	if (dmb_cell.vol_idx >= SFLC_DEV_MAX_VOLUMES) {
		sflc_log_error("The provided password opens no volume on the device");
		return EINVAL;
	}
	printf("Password is correct! Opening volumes...\n");

	/* Unlock VMBs "backwards" */
	int i;	// Needs sign, because loop ends on i>=0
	for (i = dmb_cell.vol_idx; i >= 0; i--) {
		/* Which VMB key to use? */
		char *vmb_key;
		if (i == dmb_cell.vol_idx) {
			// The one unlocked by pwd
			vmb_key = dmb_cell.vmb_key;
		} else {
			// Or the prev_vmb_key from last iteration
			vmb_key = vmbs[i+1].prev_vmb_key;
		}

		/* Read and unlock VMB */
		err = sflc_ops_readVmb(args->bdev_path, vmb_key, nr_slices, (size_t) i, &vmbs[i]);
		if (err) {
			sflc_log_error("Could not read VMB %d on device %s; error %d",
					i, args->bdev_path, err);
			return err;
		}
	}

	/* Get the ID that will be assigned to the block device */
	err = _getNextDevId(&dev_id);
	if (err) {
		sflc_log_error("Could not get next device ID; error %d", err);
		return err;
	}
	sflc_log_debug("Next device ID is %lu", dev_id);

	/* Open volumes "in order" */
	for (i = 0; i <= dmb_cell.vol_idx; i++) {
		err = sflc_ops_openVolume(args->bdev_path, dev_id, (size_t) i, &vmbs[i]);
		if (err) {
			sflc_log_error("Could not open volume %d; error %d. "
					"Previous volumes on the device might have already "
					"been opened, it's recommended you close them",
					i, err);
			return err;
		}
		sflc_log_debug("Successfully opened volume %d with VMB key", i);
		printf("Opened volume /dev/mapper/sflc_%lu_%d\n", dev_id, i);
	}

	return 0;
}


/*****************************************************
 *          PRIVATE FUNCTIONS PROTOTYPES             *
 *****************************************************/

/* Read the next device ID in sysfs */
static int _getNextDevId(size_t *next_dev_id)
{
	char *str_nextdevid;
	int err;

	/* Read sysfs entry */
	str_nextdevid = sflc_readFile(SFLC_SYSFS_NEXTDEVID);
	if (!str_nextdevid) {
		sflc_log_error("Could not read sysfs entry %s", SFLC_SYSFS_NEXTDEVID);
		return EINVAL;
	}

	/* Parse integer */
	if (sscanf(str_nextdevid, "%lu", next_dev_id) != 1) {
		sflc_log_error("Error parsing content of file %s", SFLC_SYSFS_NEXTDEVID);
		err = EINVAL;
		goto err_devid;
	}
	/* Sanity check */
	if (*next_dev_id >= SFLC_TOT_MAX_DEVICES) {
		sflc_log_error("There are already %d open devices, this is the maximum allowed", SFLC_TOT_MAX_DEVICES);
		err = E2BIG;
		goto err_devid;
	}

	/* All good */
	err = 0;


err_devid:
	free(str_nextdevid);
	return err;
}

