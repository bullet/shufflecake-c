/*
 * Copyright The Shufflecake Project Authors (2022)
 * Copyright The Shufflecake Project Contributors (2022)
 * Copyright Contributors to the The Shufflecake Project.
 *
 * See the AUTHORS file at the top-level directory of this distribution and at
 * <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *
 * This file is part of the program shufflecake-c, which is part of the
 * Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 * layer for Linux. See <https://www.shufflecake.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version. This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details. You should have received a copy of the
 * GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                 INCLUDE SECTION                  *
 *****************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include "utils/input.h"
#include "utils/log.h"


/*****************************************************
 *          PUBLIC FUNCTIONS DEFINITIONS            *
 *****************************************************/

/* Reads a line (discarding the newline) from stdin. No buffer overflow */
int sflc_safeReadLine(char *buf, size_t bufsize)
{
	size_t len;

	/* Read from stdin */
	if (fgets(buf, bufsize, stdin) == NULL) {
		sflc_log_error("Could not read from stdin");
		return EBADFD;
	}

	/* Discard newline */
	len = strlen(buf);
	if (buf[len - 1] == '\n') {
		buf[len - 1] = '\0';
	}

	return 0;
}


/* Reads a password/passphrase in a secure way (no echo) */
int sflc_safeReadPassphrase(char *buf, size_t bufsize)
{
	size_t len;
        struct termios old, new;

        fflush(stdout); // Ensure stdout is flushed is printed immediately
        
        // Turn off echoing
        tcgetattr(STDIN_FILENO, &old);
        new = old;
        new.c_lflag &= ~ECHO;
        tcsetattr(STDIN_FILENO, TCSAFLUSH, &new);
    
	/* Read from stdin */
	if (fgets(buf, bufsize, stdin) == NULL) {
	        // If reading the password failed, ensure echoing is turned back on
                tcsetattr(STDIN_FILENO, TCSAFLUSH, &old);
		sflc_log_error("Could not read from stdin");
		return EBADFD;
	}

        // Turn echoing back on
        tcsetattr(STDIN_FILENO, TCSAFLUSH, &old);
        
	/* Discard newline */
	len = strlen(buf);
	if (buf[len - 1] == '\n') {
		buf[len - 1] = '\0';
	}

	/* Newline on screen */
	printf("\n");

	return 0;
}


