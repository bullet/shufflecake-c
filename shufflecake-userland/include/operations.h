/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, either version 2 of the License, or (at your option)
 *  any later version. This program is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 *  Public License for more details. You should have received a copy of the
 *  GNU General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _OPERATIONS_H_
#define _OPERATIONS_H_


/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <stdint.h>
#include <stddef.h>

#include "header.h"
#include "utils/crypto.h"
#include "utils/math.h"


/*****************************************************
 *                 INLINE FUNCTIONS                  *
 *****************************************************/

// Size, in 4096-byte blocks, of a whole volume header (VMB+PM)
static inline size_t sflc_volHeaderSize(size_t nr_slices)
{
	// Each PosMapBlock holds up to 1024 PSIs (Physical Slice Index)
	size_t nr_pmbs = ceil(nr_slices, SFLC_SLICE_IDX_PER_BLOCK);
	// Each array holds up to 256 PosMapBlocks
	size_t nr_arrays = ceil(nr_pmbs, SFLC_BLOCKS_PER_LOG_SLICE);

	// 1 VMB, the PMBs, and the IV blocks
	return 1 + nr_pmbs + nr_arrays;
}

// Position of the VMB for the given volume
static inline uint64_t sflc_vmbPosition(size_t vol_idx, size_t nr_slices)
{
	return 1 + ((uint64_t) vol_idx) * ((uint64_t) sflc_volHeaderSize(nr_slices));
}


/*****************************************************
 *           PUBLIC FUNCTIONS PROTOTYPES             *
 *****************************************************/

/* Encrypts and writes the DMB to disk */
int sflc_ops_writeDmb(char *bdev_path, char **pwds, size_t *pwd_lens, sflc_Dmb *dmb);
/* Reads the DMB from disk and outputs the unlocked VMB key */
int sflc_ops_readDmb(char *bdev_path, char *pwd, size_t pwd_len, sflc_DmbCell *dmb_cell);
/* Reads the DMB from disk, changes the relevant DMB cell, and writes it back */
int sflc_ops_rewriteDmbCell(char *bdev_path, sflc_DmbCell *dmb_cell, char *new_pwd, size_t new_pwd_len);

/* Encrypts and writes a volume header (VMB+PM) on-disk */
int sflc_ops_writeVolumeHeader(char *bdev_path, char *vmb_key, sflc_Vmb *vmb, size_t vol_idx);
/* Reads a VMB from disk and unlocks it */
int sflc_ops_readVmb(char *bdev_path, char *vmb_key, size_t nr_slices, size_t vol_idx, sflc_Vmb *vmb);

/* Build parameter list for ctor in dm_sflc, and send DM ioctl to create virtual block device */
int sflc_ops_openVolume(char *bdev_path, size_t dev_id, size_t vol_idx, sflc_Vmb *vmb);
/* Close the volume via the appropriate ioctl to DM */
int sflc_ops_closeVolume(char *label);

#endif /* _OPERATIONS_H_ */
