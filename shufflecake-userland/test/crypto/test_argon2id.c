/*
 * Copyright The Shufflecake Project Authors (2022)
 * Copyright The Shufflecake Project Contributors (2022)
 * Copyright Contributors to the The Shufflecake Project.
 *
 * See the AUTHORS file at the top-level directory of this distribution and at
 * <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *
 * This file is part of the program shufflecake-c, which is part of the
 * Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 * layer for Linux. See <https://www.shufflecake.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version. This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details. You should have received a copy of the
 * GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                 INCLUDE SECTION                  *
 *****************************************************/

#include <string.h>
#include <stdint.h>

#include "utils/crypto.h"
#include "test_argon2id.h"
#include "minunit.h"
#include "utils/input.h"
#include "utils/log.h"


/*****************************************************
 *                 CONSTANT VARIABLES                *
 *****************************************************/

char SALT[SFLC_ARGON_SALTLEN+1] = "Poor Petrol Pump";


/*****************************************************
 *          PUBLIC FUNCTIONS DEFINITIONS            *
 *****************************************************/

char *test_argon2id()
{
	char pwd[SFLC_BIGBUFSIZE];
	char key[SFLC_CRYPTO_KEYLEN];
	int err;

	sflc_log_blue("Testing Argon2id interactively with a fixed salt");

	// Loop until user breaks out
	while (true) {
		/* Collect password */
		printf("Choose password to hash (empty to skip): ");
		err = sflc_safeReadLine(pwd, SFLC_BIGBUFSIZE);
		mu_assert("Could not read password", !err);

		/* Check if empty */
		if (strlen(pwd) == 0) {
			break;
		}

		/* Hash it */
		err = sflc_argon2id_derive(pwd, strlen(pwd), SALT, key);
		mu_assert("Could not hash password", !err);

		/* Print it */
		printf("Salt used ASCII: \"%s\". Hex:\n", SALT);
		sflc_log_hex(SALT, SFLC_ARGON_SALTLEN);
		printf("Argon2id hash (m = %d, t = %d, p = %d):\n",
				SFLC_ARGON_M, SFLC_ARGON_T, SFLC_ARGON_P);
		sflc_log_hex(key, SFLC_CRYPTO_KEYLEN);
		printf("Go check the result online\n\n");
	}

	return NULL;
}
