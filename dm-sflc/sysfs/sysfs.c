/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <linux/module.h>

#include "sysfs.h"
#include "log/log.h"


/*****************************************************
 *                    CONSTANTS                      *
 *****************************************************/

#define SFLC_SYSFS_BDEVS_ENTRY_NAME "bdevs"
#define SFLC_SYSFS_ROOT_DEVICE_NAME "sflc"
#define SFLC_SYSFS_NEXT_DEV_ID_ATTR_NAME next_dev_id


/*****************************************************
 *            PUBLIC VARIABLES DEFINITIONS           *
 *****************************************************/

/* Kobject associated to the /sys/module/dm_sflc/bdevs entry */
struct kobject * sflc_sysfs_bdevs;

/* Root device that will be every volume's parent */
struct device * sflc_sysfs_root;


/*****************************************************
 *           PRIVATE FUNCTIONS PROTOTYPES            *
 *****************************************************/

static ssize_t sflc_sysfs_showNextDevId(struct device *kdev, struct device_attribute *attr, char *buf);


/*****************************************************
 *           PRIVATE VARIABLES DEFINITIONS           *
 *****************************************************/

/* Attribute showing the next device ID */
static const struct device_attribute sflc_sysfs_nextDevIdAttr = __ATTR(
	SFLC_SYSFS_NEXT_DEV_ID_ATTR_NAME,
	0444,
	sflc_sysfs_showNextDevId,
	NULL
);


/*****************************************************
 *           PUBLIC FUNCTIONS DEFINITIONS            *
 *****************************************************/

/* Called on module load */
int sflc_sysfs_init(void)
{
	int err;

	/* Create the bdevs entry under /sys/module/dm_sflc */
	sflc_sysfs_bdevs = kobject_create_and_add(SFLC_SYSFS_BDEVS_ENTRY_NAME, &THIS_MODULE->mkobj.kobj);
	if (!sflc_sysfs_bdevs) {
		err = -ENOMEM;
		pr_err("Could not create bdevs kobject\n");
		goto err_realdevs;
	}

	/* Create the root sflc device */
	sflc_sysfs_root = root_device_register(SFLC_SYSFS_ROOT_DEVICE_NAME);
	if (IS_ERR(sflc_sysfs_root)) {
		err = PTR_ERR(sflc_sysfs_root);
		pr_err("Could not register sflc root device; error %d\n", err);
		goto err_rootdev;
	}

	/* Add next_dev_id attribute to it */
	err = device_create_file(sflc_sysfs_root, &sflc_sysfs_nextDevIdAttr);
	if (err) {
		pr_err("Could not create mapped_slices device file; error %d\n", err);
		goto err_dev_create_file;
	}

	return 0;

err_dev_create_file:
	root_device_unregister(sflc_sysfs_root);
err_rootdev:
	kobject_put(sflc_sysfs_bdevs);
err_realdevs:
	return err;
}

/* Called on module unload */
void sflc_sysfs_exit(void)
{
	root_device_unregister(sflc_sysfs_root);
	kobject_put(sflc_sysfs_bdevs);
}


/*****************************************************
 *           PRIVATE FUNCTIONS DEFINITIONS           *
 *****************************************************/

static ssize_t sflc_sysfs_showNextDevId(struct device *kdev, struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%lu\n", sflc_dev_nextId);
}

