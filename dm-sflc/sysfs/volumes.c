/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include "sysfs.h"
#include "utils/string.h"
#include "log/log.h"


/*****************************************************
 *                    CONSTANTS                      *
 *****************************************************/

#define SFLC_SYSFS_VOL_NR_SLICES_ATTR_NAME mapped_slices


/*****************************************************
 *           PRIVATE FUNCTIONS PROTOTYPES            *
 *****************************************************/

static void sflc_sysfs_volRelease(struct device * kdev);
static ssize_t sflc_sysfs_showVolNrSlices(struct device * dev, struct device_attribute * attr, char * buf);


/*****************************************************
 *           PRIVATE VARIABLES DEFINITIONS           *
 *****************************************************/

/* Attribute showing the number of slices utilised by the volume */
static const struct device_attribute sflc_sysfs_volNrSlicesAttr = __ATTR(
	SFLC_SYSFS_VOL_NR_SLICES_ATTR_NAME,
	0444,
	sflc_sysfs_showVolNrSlices,
	NULL
);


/*****************************************************
 *           PUBLIC FUNCTIONS DEFINITIONS            *
 *****************************************************/

/* Creates and registers a sysfs_Volume instance. Returns ERR_PTR() on error. */
sflc_sysfs_Volume * sflc_sysfs_volCreateAndAdd(sflc_Volume * pvol)
{
	sflc_sysfs_Volume * vol;
	int err;

	/* Allocate device */
	vol = kzalloc(sizeof(sflc_sysfs_Volume), GFP_KERNEL);
	if (!vol) {
		err = -ENOMEM;
		pr_err("Could not allocate sysfs_Volume\n");
		goto err_vol_alloc;
	}

	/* Set volume */
	vol->pvol = pvol;

	/* Set name */
	err = dev_set_name(&vol->kdev, "%s", pvol->vol_name);
	if (err) {
		pr_err("Could not set device name %s; error %d\n", pvol->vol_name, err);
		goto err_dev_set_name;
	}

	/* Set destructor */
	vol->kdev.release = sflc_sysfs_volRelease;
	/* Set parent */
	vol->kdev.parent = sflc_sysfs_root;

	/* Register */
	err = device_register(&vol->kdev);
	if (err) {
		pr_err("Could not register volume %s; error %d\n", pvol->vol_name, err);
		goto err_dev_register;
	}

	/* Add mapped_slices attribute */
	err = device_create_file(&vol->kdev, &sflc_sysfs_volNrSlicesAttr);
	if (err) {
		pr_err("Could not create mapped_slices device file; error %d\n", err);
		goto err_dev_create_file;
	}

	return vol;


err_dev_create_file:
	device_unregister(&vol->kdev);
err_dev_register:
	put_device(&vol->kdev);
err_dev_set_name:
	kfree(vol);
err_vol_alloc:
	return ERR_PTR(err);
}

/* Releases a reference to a sysfs_Device instance */
void sflc_sysfs_putVol(sflc_sysfs_Volume * vol)
{
	device_unregister(&vol->kdev);
}


/*****************************************************
 *           PRIVATE FUNCTIONS PROTOTYPES            *
 *****************************************************/

static ssize_t sflc_sysfs_showVolNrSlices(struct device * kdev, struct device_attribute * attr, char * buf)
{
	sflc_sysfs_Volume * vol = container_of(kdev, sflc_sysfs_Volume, kdev);
	sflc_Volume * pvol = vol->pvol;

	return sprintf(buf, "%u\n", pvol->mapped_slices);
}

static void sflc_sysfs_volRelease(struct device * kdev)
{
	sflc_sysfs_Volume * vol;

	/* Cast */
	vol = container_of(kdev, sflc_sysfs_Volume, kdev);

	/* Just free */
	kfree(vol);

	return;
}
