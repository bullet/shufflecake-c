/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */
 
/* 
 * Sysfs functions
 */

#ifndef _SFLC_SYSFS_SYSFS_H_
#define _SFLC_SYSFS_SYSFS_H_


/*****************************************************
 *             TYPES FORWARD DECLARATIONS            *
 *****************************************************/

/* Necessary since device.h, volume.h, and sysfs.h all include each other */

typedef struct sflc_sysfs_device_s sflc_sysfs_Device;
typedef struct sflc_sysfs_volume_s sflc_sysfs_Volume;


/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <linux/sysfs.h>
#include <linux/device.h>

#include "device/device.h"
#include "volume/volume.h"


/*****************************************************
 *                       TYPES                       *
 *****************************************************/

/* This embeds a struct kobject */
struct sflc_sysfs_device_s
{
	/* The Device it represents */
	sflc_Device   * pdev;
        /* The name of this device's subdirectoy under realdevs */
        char          * dirname;

	/* The embedded kobject */
        struct kobject  kobj;
};

/* This embeds a struct device */
struct sflc_sysfs_volume_s
{
        /* The Volume it represents */
        sflc_Volume   * pvol;

        /* The embedded device */
        struct device   kdev;
};


/*****************************************************
 *           PUBLIC VARIABLES DECLARATIONS           *
 *****************************************************/

/* Kobject associated to the /sys/module/dm_sflc/bdevs entry */
extern struct kobject * sflc_sysfs_bdevs;

/* Root device (/sys/devices/sflc) that will be every volume's parent.
   A bit overkill to have a sflc root device, but it does the trick. */
extern struct device * sflc_sysfs_root;


/*****************************************************
 *            PUBLIC FUNCTIONS PROTOTYPES            *
 *****************************************************/

/* Called on module load/unload */
int sflc_sysfs_init(void);
void sflc_sysfs_exit(void);


/* Device-related functions */

/* Creates and registers a sysfs_Device instance. Returns ERR_PTR() on error. */
sflc_sysfs_Device * sflc_sysfs_devCreateAndAdd(sflc_Device * pdev);

/* Releases a reference to a sysfs_Device instance */
void sflc_sysfs_putDev(sflc_sysfs_Device * dev);

/* Creates a symlink inside the device's subdirectory pointing to the volume's subdirectory */
int sflc_sysfs_addVolumeToDevice(sflc_sysfs_Device * dev, sflc_sysfs_Volume * vol);

/* Removes the symlink created before */
void sflc_sysfs_removeVolumeFromDevice(sflc_sysfs_Device * dev, sflc_sysfs_Volume * vol);


/* Volume-related functions */

/* Creates and registers a sysfs_Volume instance. Returns ERR_PTR() on error. */
sflc_sysfs_Volume * sflc_sysfs_volCreateAndAdd(sflc_Volume * pvol);

/* Releases a reference to a sysfs_Volume instance */
void sflc_sysfs_putVol(sflc_sysfs_Volume * vol);


#endif /* _SFLC_SYSFS_SYSFS_H_ */
