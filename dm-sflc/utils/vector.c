/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */
 
/* 
 * Implementations for all the vector utility functions
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include "vector.h"
#include "crypto/rand/rand.h"
#include "log/log.h"


/*****************************************************
 *           PUBLIC FUNCTIONS DEFINITIONS            *
 *****************************************************/

/* Shuffle a vector of u32's with the Fisher-Yates algorithm */
int sflc_vec_u32shuffle(u32 *v, u32 len)
{
	u32 i;

	for (i = len-1; i >= 1; i--) {
		/* Sample a random index from 0 to i (inclusive) */
		s32 j = sflc_rand_uniform(i+1);
		if (j < 0) {
			pr_err("Could not sample j; error %d", j);
			return j;
		}

		/* Swap v[i] and v[j] */
		u32 tmp = v[i];
		v[i] = v[j];
		v[j] = tmp;
	}

	return 0;
}
