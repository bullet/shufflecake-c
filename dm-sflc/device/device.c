/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */
 
/* 
 * This file only implements the device-related device management functions.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include "device.h"
#include "sysfs/sysfs.h"
#include "utils/vector.h"
#include "log/log.h"

#include <linux/version.h>
#include <linux/vmalloc.h>

/*****************************************************
 *                     CONSTANTS                     *
 *****************************************************/

/*****************************************************
 *           PUBLIC VARIABLES DEFINITIONS            *
 *****************************************************/

/* The next available device ID */
size_t sflc_dev_nextId;

LIST_HEAD(sflc_dev_list);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(6,4,0)
    DEFINE_SEMAPHORE(sflc_dev_mutex, 1);
#else
    DEFINE_SEMAPHORE(sflc_dev_mutex);
#endif


/*****************************************************
 *                PRIVATE VARIABLES                  *
 *****************************************************/

/* Array tracking occupation of device IDs */
static bool *sflc_dev_occupiedIds;


/*****************************************************
 *           PRIVATE FUNCTIONS PROTOTYPES            *
 *****************************************************/

/* Acquire device ID, returns false if not possible */
static bool sflc_dev_acquireId(size_t id);

/* Release device ID */
static void sflc_dev_releaseId(size_t id);

/* Initialises and pre-shuffles the PSI array */
static int sflc_dev_initAndShufflePsiArray(u32 *psi_array, u32 len);


/*****************************************************
 *           PUBLIC FUNCTIONS DEFINITIONS            *
 *****************************************************/

/* Inits global variables */
int sflc_dev_init(void)
{
	/* Allocate occupation array */
	sflc_dev_occupiedIds = kzalloc(SFLC_DEV_MAX_DEVICES_TOT, GFP_KERNEL);
	if (!sflc_dev_occupiedIds) {
		pr_err("Could not allocate array to track deviceID occupation");
		return -ENOMEM;
	}

	/* First available ID is 0 */
	sflc_dev_nextId = 0;

	return 0;
}

/* Tears down global variables */
void sflc_dev_exit(void)
{
	kfree(sflc_dev_occupiedIds);
}

/* Creates Device and adds it to the list. Returns an ERR_PTR() if unsuccessful. */
sflc_Device * sflc_dev_create(struct dm_target * ti, char * real_dev_path, u32 tot_slices)
{
	sflc_Device * dev;
	int err;
	int i;

	pr_debug("Called to create sflc_Device on %s\n", real_dev_path);

	/* Allocate device */
	dev = kzalloc(sizeof(sflc_Device), GFP_KERNEL);
	if (!dev) {
		pr_err("Could not allocate %lu bytes for sflc_Device\n", sizeof(sflc_Device));
		err = -ENOMEM;
		goto err_alloc_dev;
	}

	/* Init list node here, so it's always safe to list_del() */
	INIT_LIST_HEAD(&dev->list_node);

	/* Set device ID */
	dev->dev_id = sflc_dev_nextId;
	if (!sflc_dev_acquireId(sflc_dev_nextId)) {
		pr_err("Could not create Device: max number of open devices reached");
		err = -EINVAL;
		goto err_dev_id;
	}

	/* Set backing real device */
	err = dm_get_device(ti, real_dev_path, dm_table_get_mode(ti->table), &dev->bdev);
	if (err) {
		pr_err("Could not dm_get_device: error %d\n", err);
		goto err_dm_get_dev;
	}
	/* And its path */
	dev->bdev_path = kmalloc(strlen(real_dev_path) + 1, GFP_KERNEL);
	if (!dev->bdev_path) {
		pr_err("Could not allocate %lu bytes for dev->real_dev_path\n", strlen(real_dev_path) + 1);
		err = -ENOMEM;
		goto err_alloc_real_dev_path;
	}
	strcpy(dev->bdev_path, real_dev_path);

	/* Init volumes */
	for (i = 0; i < SFLC_DEV_MAX_VOLUMES; ++i) {
		dev->vol[i] = NULL;
	}
	dev->vol_cnt = 0;

	/* Set slices info */
	dev->tot_slices = tot_slices;
	dev->free_slices = tot_slices;
	/* Compute header info (like in userland tool) */
	u32 nr_pmbs_per_vol = DIV_ROUND_UP(tot_slices, SFLC_VOL_HEADER_MAPPINGS_PER_BLOCK);
	dev->vol_header_nr_iv_blocks = DIV_ROUND_UP(nr_pmbs_per_vol, SFLC_VOL_LOG_SLICE_SIZE);
	dev->vol_header_size = 1 + nr_pmbs_per_vol + dev->vol_header_nr_iv_blocks;
	dev->dev_header_size = 1 + (SFLC_DEV_MAX_VOLUMES * dev->vol_header_size);

	/* Init slices lock */
	mutex_init(&dev->slices_lock);
	/* Allocate reverse slice map */
	dev->rmap = vmalloc(dev->tot_slices * sizeof(u8));
	if (!dev->rmap) {
		pr_err("Could not allocate reverse slice map\n");
		err = -ENOMEM;
		goto err_alloc_rmap;
	}
	/* Initialise it */
	memset(dev->rmap, SFLC_DEV_RMAP_INVALID_VOL, dev->tot_slices * sizeof(u8));
	/* Allocate PSI array */
	dev->prmslices = vmalloc(dev->tot_slices * sizeof(u32));
	if (!dev->prmslices) {
		pr_err("Could not allocate PSI array\n");
		err = -ENOMEM;
		goto err_alloc_psi_array;
	}
	/* Initialise it and pre-shuffle it */
	err = sflc_dev_initAndShufflePsiArray(dev->prmslices, dev->tot_slices);
	if (err) {
		pr_err("Could not init-and-shuffle PSI array: error %d", err);
		goto err_initshuffle_psi_array;
	}
	/* Init related counter */
	dev->prmslices_octr = 0;

	/* Init IV cache lock */
	mutex_init(&dev->iv_cache_lock);
	/* Init IV cache waitqueue */
	init_waitqueue_head(&dev->iv_cache_waitqueue);
	/* Allocate IV cache */
	dev->iv_cache = vzalloc(dev->tot_slices * sizeof(sflc_dev_IvCacheEntry *));
	if (!dev->iv_cache) {
		pr_err("Could not allocate IV cache\n");
		err = -ENOMEM;
		goto err_alloc_iv_cache;
	}
	/* Set it empty */
	dev->iv_cache_nr_entries = 0;
	/* Init list head */
	INIT_LIST_HEAD(&dev->iv_lru_list);

	/* Create kobject */
	dev->kobj = sflc_sysfs_devCreateAndAdd(dev);
	if (IS_ERR(dev->kobj)) {
		err = PTR_ERR(dev->kobj);
		pr_err("Could not create kobject; error %d\n", err);
		goto err_sysfs;
	}

	/* Create dm_io_client */
	dev->dmio_client = dm_io_client_create();
	if (IS_ERR(dev->dmio_client)) {
		err = PTR_ERR(dev->dmio_client);
		pr_err("Could not create dm_io_client; error %d\n", err);
		goto err_dmio;
	}

	/* Add to device list */
	list_add_tail(&dev->list_node, &sflc_dev_list);

	return dev;


err_dmio:
	sflc_sysfs_putDev(dev->kobj);
err_sysfs:
	vfree(dev->iv_cache);
err_alloc_iv_cache:
err_initshuffle_psi_array:
	vfree(dev->prmslices);
err_alloc_psi_array:
	vfree(dev->rmap);
err_alloc_rmap:
	kfree(dev->bdev_path);
err_alloc_real_dev_path:
	dm_put_device(ti, dev->bdev);
err_dm_get_dev:
	sflc_dev_releaseId(dev->dev_id);
err_dev_id:
	kfree(dev);
err_alloc_dev:
	return ERR_PTR(err);
}

/* Returns NULL if not found */
sflc_Device * sflc_dev_lookupByPath(char * real_dev_path)
{
	sflc_Device * dev;

	/* Sweep the list of devices */
	list_for_each_entry(dev, &sflc_dev_list, list_node) {
		if (strcmp(real_dev_path, dev->bdev_path) == 0) {
			return dev;
		}
	}

	return NULL;
}

/* Returns false if still busy (not all volumes have been removed). Frees the Device. */
bool sflc_dev_destroy(struct dm_target * ti, sflc_Device * dev) 
{
	/* Check if we actually have to put this device */
	if (!dev) {
		return false;
	}
	if (dev->vol_cnt > 0) {
		pr_warn("Called while still holding %d volumes\n", dev->vol_cnt);
		return false;
	}

	/* Flush all IVs */
	sflc_dev_flushIvs(dev);

	/* List */
	list_del(&dev->list_node);

	/* dm_io */
	dm_io_client_destroy(dev->dmio_client);

	/* Sysfs */
	sflc_sysfs_putDev(dev->kobj);

	/* IV cache */
	vfree(dev->iv_cache);

	/* PSI array */
	vfree(dev->prmslices);

	/* Reverse slice map */
	vfree(dev->rmap);

	/* Backing device */
	dm_put_device(ti, dev->bdev);
	kfree(dev->bdev_path);

	/* Release device ID */
	sflc_dev_releaseId(dev->dev_id);

	/* Free the device itself */
	kfree(dev);

	return true;
}


/*****************************************************
 *          PRIVATE FUNCTIONS DEFINITIONS            *
 *****************************************************/

/* Acquire device ID, returns false if not possible */
static bool sflc_dev_acquireId(size_t id)
{
	/* Sanity check */
	if (id >= SFLC_DEV_MAX_DEVICES_TOT) {
		return false;
	}
	/* Check occupation */
	if (sflc_dev_occupiedIds[id]) {
		return false;
	}

	/* Mark as occupied */
	sflc_dev_occupiedIds[id] = true;

	/* Update the nextId if necessary */
	if (id == sflc_dev_nextId) {
		/* Jump to the next unoccupied ID */
		for (; id < SFLC_DEV_MAX_DEVICES_TOT && sflc_dev_occupiedIds[id]; id++);
		sflc_dev_nextId = id;
	}

	return true;
}

/* Release volume ID */
static void sflc_dev_releaseId(size_t id)
{
	/* Sanity check */
	if (id >= SFLC_DEV_MAX_DEVICES_TOT) {
		return;
	}

	/* Mark as unoccupied */
	sflc_dev_occupiedIds[id] = false;

	/* Update the nextId if necessary */
	if (id < sflc_dev_nextId) {
		sflc_dev_nextId = id;
	}

	return;
}


/* Initialises and pre-shuffles the PSI array */
static int sflc_dev_initAndShufflePsiArray(u32 *psi_array, u32 len)
{
	u32 i;

	/* Init to the identity map */
	for (i = 0; i < len; i++) {
		psi_array[i] = i;
	}

	/* Permute */
	return sflc_vec_u32shuffle(psi_array, len);
}

