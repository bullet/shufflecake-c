/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include "device.h"
#include "utils/pools.h"
#include "log/log.h"

#include <linux/bio.h>
#include <linux/dm-io.h>
#include <linux/errno.h>

/*****************************************************
 *                     CONSTANTS                     *
 *****************************************************/

/*****************************************************
 *           PRIVATE FUNCTIONS PROTOTYPES            *
 *****************************************************/

/*****************************************************
 *           PUBLIC FUNCTIONS DEFINITIONS            *
 *****************************************************/

/* Synchronously reads/writes one 4096-byte sector from/to the underlying device 
   to/from the provided page */
int sflc_dev_rwSector(sflc_Device * dev, struct page * page, sector_t sector, int rw)
{
        struct bio *bio;
        blk_opf_t opf;
        int err;

        /* Synchronous READ/WRITE */
        opf = ((rw == READ) ? REQ_OP_READ : REQ_OP_WRITE);
        opf |= REQ_SYNC;

        /* Allocate bio */
        bio = bio_alloc_bioset(dev->bdev->bdev, 1, opf,  GFP_NOIO, &sflc_pools_bioset);
        if (!bio) {
                pr_err("Could not allocate bio\n");
                return -ENOMEM;
        }

        /* Set sector */
        bio->bi_iter.bi_sector = sector * SFLC_DEV_SECTOR_SCALE;
        /* Add page */
        if (!bio_add_page(bio, page, SFLC_DEV_SECTOR_SIZE, 0)) {
                pr_err("Catastrophe: could not add page to bio! WTF?\n");
                err = EINVAL;
                goto out;
        }

        /* Submit */
        err = submit_bio_wait(bio);

out:
        /* Free and return; */
        bio_put(bio);
        return err;
}


/* Synchronously read/write entire PSI (257 blocks) to/from VMA */
int sflc_dev_rwPsi(sflc_Device *dev, void *vma, u32 psi, int rw)
{
	struct dm_io_request io_req = {
		.bi_opf = ((rw == READ) ? REQ_OP_READ : REQ_OP_WRITE) | REQ_SYNC,

		.mem.type = DM_IO_VMA,
		.mem.offset = 0,
		.mem.ptr.vma = vma,

		.notify.fn = NULL,

		.client = dev->dmio_client
	};
	struct dm_io_region io_reg = {
		.bdev = dev->bdev->bdev,
		.sector = sflc_dev_psiToIvBlock(dev, psi) * SFLC_DEV_SECTOR_SCALE,
		.count = SFLC_DEV_PHYS_SLICE_SIZE * SFLC_DEV_SECTOR_SCALE
	};

	return dm_io(&io_req, 1, &io_reg, NULL);
}

/*****************************************************
 *          PRIVATE FUNCTIONS DEFINITIONS            *
 *****************************************************/
