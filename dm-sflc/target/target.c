/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */
 
/* 
 * Methods of our DM target
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include "target.h"
#include "device/device.h"
#include "volume/volume.h"
#include "utils/bio.h"
#include "utils/string.h"
#include "log/log.h"

/*****************************************************
 *                    CONSTANTS                      *
 *****************************************************/

/*****************************************************
 *           PRIVATE FUNCTIONS PROTOTYPES            *
 *****************************************************/

static int sflc_tgt_ctr(struct dm_target *ti, unsigned int argc, char **argv);
static void sflc_tgt_dtr(struct dm_target *ti);
static int sflc_tgt_map(struct dm_target *ti, struct bio *bio);
static void sflc_tgt_ioHints(struct dm_target *ti, struct queue_limits *limits);
static int sflc_tgt_iterateDevices(struct dm_target *ti, iterate_devices_callout_fn fn,
									void *data);

/*****************************************************
 *           PUBLIC VARIABLES DEFINITIONS            *
 *****************************************************/

struct target_type sflc_target = {
	.name               = "shufflecake",
	.version            = {1, 0, 0},
	.module             = THIS_MODULE,
	.ctr                = sflc_tgt_ctr,
	.dtr                = sflc_tgt_dtr,
	.map                = sflc_tgt_map,
	.status             = NULL,
	.io_hints           = sflc_tgt_ioHints,
	.iterate_devices    = sflc_tgt_iterateDevices,
};

/*****************************************************
 *           PRIVATE FUNCTIONS DEFINITIONS           *
 *****************************************************/

/* Called every time we create a volume with the userland tool */
static int sflc_tgt_ctr(struct dm_target *ti, unsigned int argc, char **argv)
{
	char * bdev_path;
	int vol_idx;
	char * enckey_hex;
	u8 enckey[SFLC_SK_KEY_LEN];
	u32 tot_slices;
	sflc_Device * dev;
	sflc_Volume * vol;
	int err;

	/*
	* Parse arguments.
	*
	* argv[0]: underlying block device path
	* argv[1]: volume index within the device
	* argv[2]: number of 1 MB slices in the underlying device
	* argv[3]: 32-byte encryption key (hex-encoded)
	*/
	if (argc != 4) {
		ti->error = "Invalid argument count";
		return -EINVAL;
	}
	bdev_path = argv[0];
	sscanf(argv[1], "%d", &vol_idx);
	sscanf(argv[2], "%u", &tot_slices);
	enckey_hex = argv[3];

	/* Decode the encryption key */
	if (strlen(enckey_hex) != 2 * SFLC_SK_KEY_LEN) {
		pr_err("Hexadecimal key (length %lu): %s\n", strlen(enckey_hex), enckey_hex);
		ti->error = "Invalid key length";
		return -EINVAL;
	}
	err = sflc_str_hexDecode(enckey_hex, enckey);
	if (err) {
		ti->error = "Could not decode hexadecimal encryption key";
		return err;
	}

	/* Acquire the big device lock */
	if (down_interruptible(&sflc_dev_mutex)) {
		ti->error = "Interrupted while waiting for access to Device";
		return -EINTR;
	}

	/* Check if we already have a Device for this bdev */
	dev = sflc_dev_lookupByPath(bdev_path);
	if (IS_ERR(dev)) {
		ti->error = "Could not look up device by path (interrupted)";
		up(&sflc_dev_mutex);
		return PTR_ERR(dev);
	}

	/* Otherwise create it (also adds it to the device list) */
	if (!dev) {
		pr_notice("Device for %s didn't exist before, going to create it\n", bdev_path);
		dev = sflc_dev_create(ti, bdev_path, tot_slices);
	} else {
		pr_notice("Device on %s already existed\n", bdev_path);
	}
	
	/* Check for device creation errors */
	if (IS_ERR(dev)) {
		ti->error = "Could not create device";
		up(&sflc_dev_mutex);
		return PTR_ERR(dev);
	}

	/* Create the volume (also adds it to the device) */
	vol = sflc_vol_create(ti, dev, vol_idx, enckey);
	if (IS_ERR(vol)) {
		ti->error = "Error creating volume";
		up(&sflc_dev_mutex);
		return PTR_ERR(vol);
	}
	pr_debug("Now %d volumes are linked to device %s\n", dev->vol_cnt, bdev_path);

	/* Release the big device lock */
	up(&sflc_dev_mutex);

	/* Tell DM we want one SFLC sector at a time */
	ti->max_io_len = SFLC_DEV_SECTOR_SCALE;
	/* Enable REQ_OP_FLUSH bios */
	ti->num_flush_bios = 1;
	/* Disable REQ_OP_WRITE_ZEROES and REQ_OP_SECURE_ERASE (can't be passed through as
	   they would break deniability, and they would be too complicated to handle individually) */
	ti->num_secure_erase_bios = 0;
	ti->num_write_zeroes_bios = 0;
	/* Momentarily disable REQ_OP_DISCARD_BIOS
	   TODO: will need to support them to release slice mappings */
	ti->num_discard_bios = 0;
	/* When we receive a ->map call, we won't need to take the device lock anymore */
	ti->private = vol;

	return 0;
}

/* Called every time we destroy a volume with the userland tool */
static void sflc_tgt_dtr(struct dm_target *ti)
{
	sflc_Volume * vol = ti->private;
	sflc_Device * dev = vol->dev;

	pr_debug("Destroying volume \"%s\"\n", vol->vol_name);

	/* We do need to take the device lock here, as we'll be modifying the device */
	if (down_interruptible(&sflc_dev_mutex)) {
		pr_err("Interrupted while waiting to destroy volume\n");
		return;
	}

	/* Destroy volume (also decreases refcount in device) */
	sflc_vol_destroy(ti, vol);

	/* Destroy the device */
	if (dev->vol_cnt == 0) {
		pr_notice("Removed the last volume from device\n");
		sflc_dev_destroy(ti, dev);
	}

	/* End of critical section */
	up(&sflc_dev_mutex);

	return;
}

/* Callback for every bio submitted to our virtual block device */
static int sflc_tgt_map(struct dm_target *ti, struct bio *bio) 
{
	int err;
	sflc_Volume * vol = ti->private;

        /* If no data, just quickly remap the sector and the block device (no crypto) */
	/* TODO: this is dangerous for deniability, will need more filtering */
        if (unlikely(!bio_has_data(bio))) {
        	pr_debug("No-data bio: bio_op = %d", bio_op(bio));
                err = sflc_vol_remapBioFast(vol, bio);
		if (err) {
			pr_err("Could not remap bio; error %d\n", err);
			return DM_MAPIO_KILL;
		}
                return DM_MAPIO_REMAPPED;
        }

	/* At this point, the bio has data. Do a few sanity checks */
	/* TODO: I think we can get rid of all of them */

        /* Check that it is properly aligned and it doesn't cross vector boundaries */
        if (unlikely(!sflc_bio_isAligned(bio))) {
                pr_err("Unaligned bio!\n");
                return DM_MAPIO_KILL;
        }
        /* If it contains more than one SFLC sector, complain with the DM layer and continue */
        if (unlikely(bio->bi_iter.bi_size > SFLC_DEV_SECTOR_SIZE)) {
		pr_notice("Large bio of size %u\n", bio->bi_iter.bi_size);
                dm_accept_partial_bio(bio, SFLC_DEV_SECTOR_SCALE);
        }
	/* Check that it contains exactly one SFLC sector */
        if (unlikely(bio->bi_iter.bi_size != SFLC_DEV_SECTOR_SIZE)) {
                pr_err("Wrong length (%u) of bio\n", bio->bi_iter.bi_size);
                return DM_MAPIO_KILL;
        }

	/* Now it is safe, process it */
	err = sflc_vol_processBio(vol, bio);
        if (err) {
                pr_err("Could not enqueue bio\n");
                return DM_MAPIO_KILL;
        }

	return DM_MAPIO_SUBMITTED;
}

/* Callback executed to inform the DM about our 4096-byte sector size */
static void sflc_tgt_ioHints(struct dm_target *ti, struct queue_limits *limits)
{
	sflc_Volume * vol = ti->private;

	pr_info("Called io_hints on volume \"%s\"\n", vol->vol_name);

	limits->logical_block_size = SFLC_DEV_SECTOR_SIZE;
	limits->physical_block_size = SFLC_DEV_SECTOR_SIZE;

	limits->io_min = SFLC_DEV_SECTOR_SIZE;
	limits->io_opt = SFLC_DEV_SECTOR_SIZE;

	return;
}

/* Callback needed for God knows what, otherwise io_hints never gets called */
static int sflc_tgt_iterateDevices(struct dm_target *ti, iterate_devices_callout_fn fn,
									void *data)
{
	sflc_Volume * vol = ti->private;
	sflc_Device * dev = vol->dev;

	pr_debug("Called iterate_devices on volume \"%s\"\n", vol->vol_name);

	if (!fn) {
		return -EINVAL;
	}
	return fn(ti, vol->dev->bdev, 0, 
		(dev->dev_header_size + dev->tot_slices * SFLC_DEV_PHYS_SLICE_SIZE) * SFLC_DEV_SECTOR_SCALE,
		data);
}
